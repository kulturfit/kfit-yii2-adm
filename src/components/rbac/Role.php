<?php

namespace kfit\adm\components\rbac;

use kfit\adm\models\app\AuthItem;

class Role extends AuthItem
{
    /**
     * {@inheritdoc}
     */
    public $type = AuthItem::TYPE_ROL;
}

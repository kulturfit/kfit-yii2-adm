<?php

namespace kfit\adm\components;

use yii\web\AssetBundle;

/**
 * AssetBundle AdmAssets.
 *
 * @package kfit/adm
 * @subpackage components
 * @category AssetBundle
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class AdmAssets extends AssetBundle
{
    public $sourcePath = '@kfit/adm/assets/';
    public $css = [
        'module.css'
    ];
    public $js = [
        'module.js',
    ];
    public $depends = [
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'kfit\core\web\BaseAsset'
    ];
}

<?php

namespace kfit\adm\components;

use kfit\core\base\Model as ModelBase;
use Yii;
use kfit\adm\Module;

/**
 * Modelo base
 *
 * @package kfit/adm
 * @subpackage components
 * @category Components
 *
 * @property string $titleReportExport Almacena el nombre del reporte generado con kartik-yii2-export
 * @property-read string STATUS_ACTIVE Estado activo segÃºn la columna.
 * @property-read string STATUS_INACTIVE Estado inactivo segÃºn la columna.
 * @property-read string STATUS_COLUMN Nombre de columna de estado.
 * @property-read string CREATED_AT_COLUMN Nombre de columna de Creado por.
 * @property-read string CREATED_DATE_COLUMN Nombre de columna de Fecha creaciÃ³n.
 * @property-read string UPDATED_AT_COLUMN Nombre de columna de Modificado por.
 * @property-read string UPDATED_DATE_COLUMN Nombre de columna de Fecha modificaciÃ³n.
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Juan Sebastian MuÃ±oz Reyes <juan.munoz@ticmakers.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class Model extends ModelBase
{
    /**
     * Configuración inicial.
     *
     * @return null
     */
    public function init()
    {
        parent::init();
        $this->module = (Module::getInstance()) ?? Yii::$app;
    }
}

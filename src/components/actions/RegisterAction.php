<?php

namespace kfit\adm\components\actions;

use kfit\adm\components\actions\BaseAction;

use Yii;
use yii\web\Response;
use yii\helpers\Url;
use kfit\core\helpers\MessageHelper;

/**
 * Acción RegisterAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class RegisterAction extends BaseAction
{

    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = '_register';

    /**
     * Permite ejecutar la acción para registrar un usuario
     *
     * @return mixed
     */
    public function run()
    {
        $model = Yii::createObject(['class' => Yii::$app->user->identityClass]);
        $model->scenario = $model::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if ($model->sendEmailConfirmation($model->email)) {
                    if ($this->isModal) {
                        return [
                            'state' => Yii::$app->message::TYPE_SUCCESS,
                            'message' => Yii::t($this->module->id, 'It was created successfully'),
                            'type' => 'open-load-modal',
                            'modal' => 'default-modal',
                            'errors' => '',
                            'url' => Url::toRoute(['site/confirmation', 'email'  => $model->email, true]),
                        ];
                    } else {
                        Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($this->module->id, 'It was created successfully'));
                        return Yii::$app->response->redirect(Yii::$app->user->loginUrl);
                    }
                } else {
                    if ($this->isModal) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'state' => Yii::$app->message::TYPE_DANGER,
                            'type' => 'message',
                            'errors' => Yii::t($this->module->id, 'There was an error trying to send the confirmation email'),
                            'redirect' => false,
                            'url' => null,
                            'modal' => null
                        ];
                    } else {
                        Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'There was an error trying to send the confirmation email'));
                    }
                }
            } else {

                if ($this->isModal) {
                    $model->validate();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'state' => Yii::$app->message::TYPE_DANGER,
                        'type' => 'message',
                        'errors' => Yii::$app->html::errorSummary($model),
                        'redirect' => false,
                        'url' => null,
                        'modal' => null
                    ];
                } else {
                    Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'There was an error trying to create'));
                }
            }
        }

        return $this->render(
            $this->_viewFile,
            [
                'model' => $model,
                'moduleId' => $this->module->id,
            ]
        );
    }
}

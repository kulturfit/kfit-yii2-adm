<?php

namespace kfit\adm\components\actions;

use kfit\adm\components\actions\BaseAction;
use kfit\adm\models\base\Users;
use yii\web\Response;
use Yii;

/**
 * Acción ActiveAccountAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class ActiveAccountAction extends BaseAction
{

    /**
     * Permite ejecutar la acción para registrar un usuario
     *
     * @return mixed
     */
    public function run($token)
    {
        Yii::$app->response->format = Response::FORMAT_HTML;
        $modelObject = Yii::createObject(['class' => Yii::$app->user->identityClass]);
        $user = $modelObject::findByEmailKey($token);

        if (!empty($user)) {
            if ($user->confirmed_email == Users::STATUS_INACTIVE) {
                $user->confirmed_email = Users::STATUS_ACTIVE;
                $user->scenario = Users::SCENARIO_ACTIVE;
                if ($user->update()) {
                    Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t($this->module->id, 'Your account has been confirmed'));
                } else {
                    Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'An error occurred when trying to activate the account'));
                }
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_WARNING, Yii::t($this->module->id, 'Your account has already been confirmed. Now you can go to login'));
            }
        } else {
            Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t($this->module->id, 'There is no user associated with your confirmation code'));
        }

        return Yii::$app->response->redirect(Yii::$app->user->loginUrl);
    }
}

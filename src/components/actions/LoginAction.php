<?php

namespace kfit\adm\components\actions;

use Yii;
use kfit\adm\components\actions\BaseAction;
use yii\web\Response;
use kfit\adm\models\forms\Login;
use kfit\adm\models\base\SocialNetworks;

/**
 * LoginAction clase tipo acción que puede ser implementada en un controlador para el manejo del login de los usuarios.
 *
 * @property string $view Vista que se renderizará para el formulario de login.
 *
 * @package kfit
 * @subpackage adm\components\actions
 *
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class LoginAction extends BaseAction
{

    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = '_login';

    /**
     * Modelo que se utiliza para las validaciones del formulario
     * 
     * @var string
     */
    public $formModel = Login::class;

    /**
     * Modelo que se utiliza para el manejo de la información por redes sociales.
     * 
     * @var string
     */
    public $socialModel = SocialNetworks::class;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $loginGoogle = false;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $loginFacebook = false;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $register = true;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isMultiModel = false;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isRedis = false;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $formInTabs = false;

    /**
     * Undocumented variable
     *
     * @var array
     */
    public $tabsConfig = [];

    /**
     * Permite ejecutar la acción para registrar un usuario
     *
     * @return mixed
     */
    public function run()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->getResponseLogIn();
        }
        if (empty(Yii::$app->request->post('social_network_user'))) {
            return $this->login();
        }
    }

    /**
     * Inicio de sesión normal
     */
    public function login()
    {
        $modelInstance = Yii::createObject($this->formModel);
        if ($modelInstance->load(Yii::$app->request->post()) && $modelInstance->login()) {
            return $this->getResponseLogIn();
        }
        return $this->getNormalResponse($modelInstance);
    }

    /**
     * Manejador de respuesta cuando está loggeado el usuario
     */
    public function getResponseLogIn()
    {
        if ($this->isModal) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'state' => Yii::$app->message::TYPE_SUCCESS,
                'message' => Yii::t($this->module->id, 'Welcome user.'),
                'errors' => '',
                'type' => 'redirect', //open-modal, open-load-modal, redirect, message
                'url' => Yii::$app->request->referrer ?? Yii::$app->getHomeUrl(),
                'modal' => ''
            ];
        }
        return $this->controller->goHome();
    }

    /**
     * Manejador de respuesta cuento no es carga o hay errores
     */
    public function getNormalResponse($modelInstance)
    {
        Yii::$app->response->format = Response::FORMAT_HTML;
        if ($this->isModal) {
            if ($modelInstance->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'state' => Yii::$app->message::TYPE_DANGER,
                    'message' => Yii::t($this->module->id, 'Error en el login.'),
                    'errors' => Yii::$app->html::errorSummary($modelInstance),
                    'type' => 'message', //open-modal, open-load-modal, redirect, message
                    'url' => null,
                    'modal' => null
                ];
            }
            return $this->render($this->_viewFile, [
                'model' => $modelInstance
            ]);
        }
        return $this->render($this->_viewFile, [
            'model' => $modelInstance
        ]);
    }
}

<?php

namespace kfit\adm\components\actions;

use kfit\adm\components\actions\BaseAction;
use kfit\core\components\DynamicModel;

use Yii;
use yii\web\Response;
use yii\helpers\Url;

/**
 * Acción RegisterAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class RestorePasswordAction extends BaseAction
{

    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = '_restore_password';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $redirectOnSuccess = false;

    /**
     * Permite ejecutar la acción para enviar el correo de restauración de contraseña
     *
     * @return mixed
     */
    public function run()
    {

        $modelObject = new DynamicModel(['email']);
        $modelObject->addRule(
            'email',
            'match',
            ['pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i']
        )
            ->addRule('email', 'string', ['max' => 50])
            ->addRule('email', 'required');

        $modelObject->setAttributeLabels(['email' => Yii::t('adm', 'Email')]);

        if ($modelObject->load(Yii::$app->request->post())) {
            $modelUser = Yii::createObject(['class' => Yii::$app->user->identityClass]);
            $user = $modelUser::findByName($modelObject->email);

            if (!empty($user)) {
                if ($modelUser->sendRestorePasswordEmail($user)) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    if ($this->isModal) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'state' => null,
                            'message' => null,
                            'errors' => null,
                            'type' => 'open-load-modal',
                            'url' => Url::to(['confirm-restore-password', 'user' => $user->primaryKey]),
                            'modal' => 'default-modal'
                        ];
                    } else {
                        return $this->render('confirm_change_password', ['model' => $user]);
                    }
                } else {
                    if ($this->isModal) {
                        return [
                            'state' => Yii::$app->message::TYPE_DANGER,
                            'message' => Yii::t('adm', 'an error occurred while trying to send the mail'),
                            'errors' => null,
                            'type' => 'message',
                            'url' => null,
                            'modal' => 'default-modal'
                        ];
                    } else {
                        Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('adm', 'an error occurred while trying to send the mail'));
                    }
                }
            } else {

                if ($this->isModal) {
                    return [
                        'state' => Yii::$app->message::TYPE_DANGER,
                        'message' => Yii::t('adm', 'This email is not registered, please try again'),
                        'errors' => null,
                        'type' => 'message',
                        'url' => null,
                        'modal' => null
                    ];
                } else {

                    $modelObject->addError('email', Yii::t('adm', 'This email is not registered, please try again'));

                    // Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('adm', 'This email is not registered, please try again'));
                }
            }
        }

        return $this->render(
            '_restore_password',
            [
                'model' => $modelObject,
                'moduleId' => 'adm',
            ]
        );
    }
}

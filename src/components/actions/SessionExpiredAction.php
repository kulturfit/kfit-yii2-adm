<?php

namespace kfit\adm\components\actions;

use kfit\adm\models\app\Users;
use Yii;
use yii\web\Response;

/**
 * Acción SessionExpiredAction Permite definir el comportamiento para la expiración de la sesión en el sistema
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class SessionExpiredAction extends BaseAction
{
    public function run()
    {
        $urlAssets = Yii::$app->assetManager->getPublishedUrl('@kfit/adm/assets');
        // get the cookie collection (yii\web\CookieCollection) from the "request" component
        $cookies = Yii::$app->request->getCookies();
        $returnUrl = $cookies->getValue('user_returnUrl', Yii::$app->homeUrl);
        $modelObject = Yii::createObject(['class' => Yii::$app->user->identityClass]);
        $model = $modelObject->findByUsernameOrEmail($cookies->getValue(Yii::$app->user->cookieUserEmail, ''));

        if ($model) {
            $model->scenario = Users::SCENARIO_EXPIRED;
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validatePassword($model->password)) {
                    Yii::$app->getUser()->login($model, $this->module->timeExpiredSession ?? 0);
                    if ($this->isModal) {
                        return [
                            'state' => Yii::$app->message::TYPE_DANGER,
                            'message' => Yii::t($this->module->id, ''),
                            'errors' => '',
                            'type' => 'redirect', //open-modal, open-load-modal, redirect, message
                            'url' => $returnUrl,
                            'modal' => '',
                        ];
                    } else {
                        return Yii::$app->response->redirect($returnUrl);
                    }
                } else {
                    $model->addError('password', Yii::t($this->module->id, 'The password entered is not correct.'));
                }
            }
            Yii::$app->response->format = Response::FORMAT_HTML;
            if ($this->isModal) {
                return $this->controller
                    ->renderAjax('_session_expired', [
                        'model' => $model,
                        'moduleId' => $this->module->id,
                        'urlAssets' => $urlAssets,
                    ]);
            } else {
                return $this->controller
                    ->render('_session_expired', [
                        'model' => $model,
                        'moduleId' => $this->module->id,
                        'urlAssets' => $urlAssets,
                    ]);
            }
        } else {
            if ($this->isModal) {
                return [
                    'state' => Yii::$app->message::TYPE_DANGER,
                    'message' => Yii::t($this->module->id, 'Session expired'),
                    'errors' => '',
                    'type' => 'open-modal', //open-modal, open-load-modal, redirect, message
                    'url' => Yii::$app->user->loginUrl,
                    'modal' => 'login-modal',
                ];
            } else {
                return Yii::$app->response->redirect(Yii::$app->user->loginUrl);
            }
        }
    }
}

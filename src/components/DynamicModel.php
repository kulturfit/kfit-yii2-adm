<?php

namespace kfit\adm\components;

use kfit\core\components\DynamicModel as DynamicModelBase;

/**
 * Modelo base
 *
 * @package kfit
 * @subpackage adm
 * @category Components
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class DynamicModel extends DynamicModelBase
{ }

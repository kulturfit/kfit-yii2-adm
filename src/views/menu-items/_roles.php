<?php

use kfit\adm\models\app\AuthItem;
use kfit\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kfit\adm\models\RoleMenuItems */
/* @var $form kfit\core\widgets\ActiveForm */
?>

<div class="menu-items">
    <?php $form = ActiveForm::begin([
        'id' => 'create-update-form-menu-items',
    ]); ?>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'roles', [
                'help' => '',
                // 'popover' => $model->getHelp('roles')
            ])->widget(
                \kartik\widgets\Select2::classname(),
                [
                    'data' => AuthItem::getData(true, ['type' => AuthItem::TYPE_ROL]),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty(),
                        'tabindex' => 1,
                        'multiple' => true,
                    ],
                ]
            ) ?>

        </div>

    </div> <?php ActiveForm::end(); ?>
</div>
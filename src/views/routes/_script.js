
$('#btn-new').click(function () {
    var $this = $(this);
    let routeName = $('#inp-route').val().trim();
    if (routeName && routeName != '') {
        $this.children('i.glyphicon-refresh-animate').show();
        $.post($this.attr('href'), {route: routeName}, function (res) {
            if (res.status){
                showSuccessMessage(res.message);
                updateItems(res);
                $('#inp-route').val('').focus();
            }else{
                showErrorMessage(res.message);
            }
            
        }).always(function () {
            $this.children('i.glyphicon-refresh-animate').hide();
        });
    }
    return false;
});

$('.btn-assign').click(function () {
    var $this = $(this);
    var target = $this.data('target');
    var routes = $('select.list[data-target="' + target + '"]').val();

    if (routes && routes.length) {
        $this.children('i.glyphicon-refresh-animate').show();
        $.post($this.attr('href'), {routes: routes}, function (routes) {
            updateItems(routes);
        }).always(function () {
            $this.children('i.glyphicon-refresh-animate').hide();
        });
    }
    return false;
});

$('#btn-refresh').click(function () {
    let urlTarget = $(this).data('target');
    $(this).children('i.glyphicon-refresh-animate').show();
    $.post(urlTarget, function (routes) {
        updateItems(routes);
    }).always(function () {
        $(this).children('i.glyphicon-refresh-animate').hide();
    });

    return false;
});

$('.search[data-target]').keyup(function () {
    search($(this).data('target'));
}); 

function showItems(list){
    $('i.glyphicon-refresh-animate').hide();
    $('#assigned-list, #available-list').html('')
    if(list.routes.available.length > 0){ 
        $('#available-list').append('<optgroup label="Routes"></optgroup>')  
        $.each( list.routes.available, function( id, item ) {
            $("#available-list > optgroup").append('<option value="' + item.name + '">' + item.name + '</option>');
        }); 
    }
    if(list.routes.assigned.length > 0){     
        $('#assigned-list').append('<optgroup label="Routes"></optgroup>') 
        $.each( list.routes.assigned, function( id, item ) {
            $("#assigned-list > optgroup").append('<option value="' + item.name + '">' + item.name + '</option>');
        });
    }
}

function updateItems(route) {  
    _items.routes.available = route.routes.available;
    _items.routes.assigned = route.routes.assigned;
    search('available');
    search('assigned');
}

function search(target) {
    var $list = $('select.list[data-target="' + target + '"]');
    $list.html('');
    var q = $('.search[data-target="' + target + '"]').val().toLowerCase();
    var groups = {
        role: [$('<optgroup label="Routes">'), false],
    };
    $.each(_items.routes[target], function (itemName, group) {
        let currentName = group.name.toLowerCase();
        if (currentName.indexOf(q) >= 0) {
            $('<option>').text(group.name).val(group.name).appendTo(groups.role[0]);
            groups.role[1] = true;
        }
    });
    $.each(groups, function () {
        if (this[1]) {
            $list.append(this[0]);
        }
    });
} 

showItems(_items)

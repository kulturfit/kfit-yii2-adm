<?php

use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\web\YiiAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */

YiiAsset::register($this);

$titleInGrid = $titleInGrid ?? true;

if ($this->context->moduleTitle) {
    $this->moduleTitle = Yii::t('app', $this->context->moduleTitle);
    $this->params['breadcrumbs'][] = $this->moduleTitle;
}

$this->title = Yii::t('app', '{title}', [
    'title' => Yii::t('app', $searchModel::crudTitle())
]);
$this->params['breadcrumbs'][] = ['label' => $searchModel::crudTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
$id = Inflector::slug($searchModel::crudTitle());

$items = Json::htmlEncode([
    'routes' => $searchModel->itemsAssignment
]);
$this->registerJs("var _items = {$items};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
?>



<div class="<?= $id ?>-create card">
    <div class="card-body">
        <?php if ($titleInGrid) : ?>
            <div class="d-flex align-items-start justify-content-between mb-4">
                <h4><?= Yii::$app->html::encode($this->title) ?></h4>
            </div>
        <?php endif ?>

        <div class="assignment-index">

            <div class="row mb-15">
                <div class="col-12">
                    <div class="input-group">
                        <input id="inp-route" type="text" class="form-control mr-5" placeholder="<?= Yii::t('adm', 'New route(s)'); ?>">
                        <span class="input-group-btn">
                            <?= Yii::$app->html::a(Yii::t('adm', 'Add') . $animateIcon, ['create-route'], [
                                'class' => 'btn btn-success',
                                'id' => 'btn-new',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <input class="form-control search" data-target="available" placeholder="<?= Yii::t('adm', 'Search for available'); ?>">
                    <select id="available-list" multiple size="20" class="form-control list" data-target="available">
                    </select>
                </div>

                <div class="col-1 d-flex flex-column justify-content-center align-items-center">

                    <?= Yii::$app->html::a('&gt;&gt;' . $animateIcon, ['assign', 'id' => $searchModel->name], [
                        'class' => 'btn btn-success btn-assign',
                        'data-target' => 'available',
                        'title' => Yii::t('adm', 'Assign'),
                    ]); ?>

                    <?= Yii::$app->html::a('&lt;&lt;' . $animateIcon, ['revoke', 'id' => $searchModel->name], [
                        'class' => 'btn btn-danger btn-assign mt-20',
                        'data-target' => 'assigned',
                        'title' => Yii::t('adm', 'Remove'),
                    ]); ?>
                </div>

                <div class="col">
                    <input class="form-control search" data-target="assigned" placeholder="<?= Yii::t('adm', 'Search for assigned'); ?>">
                    <select id="assigned-list" multiple size="20" class="form-control list" data-target="assigned">
                    </select>
                </div>
            </div>
        </div>


    </div>
</div>




<?php

$this->registerCss('
.glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -ms-animation: spin .7s infinite linear;
    -webkit-animation: spinw .7s infinite linear;
    -moz-animation: spinm .7s infinite linear;
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
  
@-webkit-keyframes spinw {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@-moz-keyframes spinm {
    from { -moz-transform: rotate(0deg);}
    to { -moz-transform: rotate(360deg);}
}');

?>
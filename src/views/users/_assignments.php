<?php
/*
use mdm\admin\AnimateAsset; */

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\YiiAsset;
use kfit\adm\models\app\AuthItem;
/* @var $this yii\web\View */
/* @var $model mdm\admin\models\Assignment */
/* @var $fullnameField string */

YiiAsset::register($this);

$items = Json::htmlEncode([
    'roles' => $modelsFormColums['model']->itemsAssignment,
]);

$this->registerJs("var _items = {$items};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';
?>
<div class="assignment-index">

    <div class="row">
        <div class="col">
            <input class="form-control search" data-target="available" placeholder="<?= Yii::t('adm', 'Search for available'); ?>">
            <select id="available-list" multiple size="20" class="form-control list" data-target="available">
            </select>
        </div>

        <div class="col-1 d-flex flex-column justify-content-center align-items-center">

            <?= Yii::$app->html::a('&gt;&gt;' . $animateIcon, ['auth-assignment/assign', 'id' => $modelsFormColums['model']->user_id], [
                'class' => 'btn btn-success btn-assign',
                'data-target' => 'available',
                'title' => Yii::t('adm', 'Assign'),
            ]); ?>

            <?= Yii::$app->html::a('&lt;&lt;' . $animateIcon, ['auth-assignment/revoke', 'id' => $modelsFormColums['model']->user_id], [
                'class' => 'btn btn-danger btn-assign mt-20',
                'data-target' => 'assigned',
                'title' => Yii::t('adm', 'Remove'),
            ]); ?>
        </div>

        <div class="col">
            <input class="form-control search" data-target="assigned" placeholder="<?= Yii::t('adm', 'Search for assigned'); ?>">
            <select id="assigned-list" multiple size="20" class="form-control list" data-target="assigned">
            </select>
        </div>
    </div>
</div>



<?php if (!($this->context->action->isModal ?? false)) : ?>

    <div class="form-group float-right mt-5">
        <?= Yii::$app->ui::btnCancel(); ?>

        <?php if ($hasPrevious) {
                echo Yii::$app->html::button(
                    '<i class="fa fa-arrow-left"></i> ' . Yii::t('app', 'Previous'),
                    ['class' => 'btn btn-info', 'onclick' => "directionTab(null, 'L')"]
                );
            } ?>
        <?php if ($hasNext) {
                echo Yii::$app->html::button(
                    '<i class="fa fa-arrow-right"></i> ' . Yii::t('app', 'Next'),
                    ['class' => 'btn btn-info', 'onclick' => "directionTab()"]
                );
            } ?>
        <?= Yii::$app->ui::btnSend(null, ['form' => $form->id]); ?>
    </div>
<?php endif; ?>


<?php
$script = <<<JS
    function directionTab(tabId, direction){
        if(!tabId){ tabId = '.nav-tabs' }
        let item = $(tabId).find('a.active').parent()
        item = (direction && direction == 'L') ? item.prev() : item.next()
        item.find('a').click();
    }

JS;
$this->registerJs($script, yii\web\View::POS_BEGIN);
?>
<?php

$this->registerCss('
.glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -ms-animation: spin .7s infinite linear;
    -webkit-animation: spinw .7s infinite linear;
    -moz-animation: spinm .7s infinite linear;
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
  
@-webkit-keyframes spinw {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@-moz-keyframes spinm {
    from { -moz-transform: rotate(0deg);}
    to { -moz-transform: rotate(360deg);}
}');

?>
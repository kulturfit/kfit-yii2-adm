<?php

use kfit\core\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $form kfit\core\widgets\ActiveForm */
/* @var $model app\models\registerForm */

if (!isset($modelsFormColums)) {
    $modelsFormColums = ['model' => $model];
}
$isModal = $this->context->action->isModal;
$model = $modelsFormColums['model'];


$this->title = Yii::t('adm', 'Restore password');

?>

<div class="security-restore-password">
    <?php $form = ActiveForm::begin([
        'id' => 'restore-password-form',
    ]); ?>

    <?php

    $titleHtml = Yii::$app->html::tag('p', Yii::t('adm', 'Recover your password by entering the following data:'));
    $emailHtml = $form->field($model, 'email')->textInput(['autofocus' => true]);
    $btnRestoreHtml = Yii::$app->html::submitButton(Yii::t('adm', 'Restore password'), ['class' => 'btn btn-primary btn-block btn-md', 'name' => 'register-button']);

    $linkReturnHtml = Yii::$app->html::a(Yii::t('adm', 'Return to login'), Url::to(['security/login']));

    $html = Yii::$app
        ->getView()
        ->render(
            $this->context->loadViewFile('_restore_password_tpl'),
            [
                'form' => $form,
                'model' => $modelsFormColums['model']
            ],
            $this->context
        );


    $html = str_replace('{{title}}', $titleHtml, $html);
    $html = str_replace('{{email}}', $emailHtml, $html);
    $html = str_replace('{{btn-restore-password}}', $btnRestoreHtml, $html);
    $html = str_replace('{{link-return-login}}', $linkReturnHtml, $html);

    echo $html;

    ?>

    <?php ActiveForm::end(); ?>

</div>
<?php if (!$this->context->action->isModal) : ?>

    <div class="row justify-content-center align-items-center align-self-center m-0" style="height: 100%">
        <div class="col col-6 align-self-center align-middle">
            <div class="vertical-align" data-animsition-in="fade-in" data-animsition-out="fade-out">
                <div class="vertical-align-middle">
                    <div class="content-fluid"></div>
                    <div class="card px-20 py-25">
                        <div class="card-body pt-15">
                            <h2 class="card-title mb-30 text-center">{{title}}</h2>
                            <div class="site-login mt-10">

                            <?php endif ?>
                            <div class="row">
                                <div class="col-12">
                                    {{username}}
                                </div>
                                <div class="col-12">
                                    {{password}}
                                </div>
                                <div class="col-6">
                                    {{rememberMe}}
                                </div>
                                <div class="col-6 text-right">
                                    {{link-forgotPassword}}
                                </div>
                                <div class="col-12">
                                    {{btn-login}}
                                </div>

                                <?php if ($this->context->action->loginGoogle || $this->context->action->loginFacebook) : ?>
                                    {{separator-login}}
                                <?php endif; ?>
                                <?php if ($this->context->action->loginGoogle) : ?>
                                    <div class="col-12">
                                        {{btn-google}}
                                    </div>
                                <?php endif; ?>
                                <?php if ($this->context->action->loginFacebook) : ?>
                                    <div class="col-12">
                                        {{btn-facebook}}
                                    </div>
                                <?php endif; ?>
                            </div>

                            <?php if (!$this->context->action->isModal) : ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
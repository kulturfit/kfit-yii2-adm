<?php

use kfit\core\base\Modal;
use kfit\core\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $form kfit\core\widgets\ActiveForm */
/* @var $model app\models\registerForm */

$this->title = Yii::t('adm', 'Session expired');

?>

<div class="row justify-content-center align-items-center align-self-center m-0" style="height: 100%">

    <div class="col col-md-12 align-self-center align-middle">
        <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
                <div class="content-fluid"></div>
                <div class="card my-5 px-5 pb-5">
                    <div class="card-body">

                        <div>
                            <div class="row">
                                <div class="col-3">
                                    <span class="avatar avatar-online">
                                        {{img-profile}}
                                    </span>
                                </div>
                                <div class="col-9">
                                    {{title}}
                                </div>
                            </div>
                        </div>

                        <div class="security-expired-session">

                            <div class="row pt-5">
                                <div class="col-12 text-left">
                                    {{model.email:1}}
                                </div>
                                <div class="col-12 text-left">
                                    {{model.password:2}}
                                </div>

                            </div>

                            <div class="col-lg-12 mt-2">

                                <div class="container-button col-8 offset-md-2">
                                    {{btn-login}}
                                </div>

                            </div>
                            <div class="col-lg-12 mt-2">
                                {{link-returnlogin}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
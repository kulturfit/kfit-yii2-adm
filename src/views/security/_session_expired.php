<?php

use kfit\core\base\Modal;
use kfit\core\widgets\ActiveForm;
use yii\helpers\Url;

use kfit\core\helpers\ConstantsHelper;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $form kfit\core\widgets\ActiveForm */
/* @var $model app\models\registerForm */

if (!isset($modelsFormColums)) {
    $modelsFormColums = ['model' => $model];
}
$isModal = $this->context->action->isModal;
$model = $modelsFormColums['model'];

$this->title = Yii::t($moduleId, 'Session expired');

$btnLoginHtml = Yii::$app->html::submitButton(
    Yii::t('adm', 'Log in'),
    [
        'class' => 'btn btn-primary btn-block btn-md mt-10',
        'name' => 'login-button'
    ]
);

$titleHtml = "<h3>" . Yii::t('adm', 'YOUR SESSION HAS EXPIRED') . "</h3>
                <p>" . Yii::t('adm', 'Due to inactivity, your session has been closed') . "</p>
                                    <p>" . Yii::t('adm', 'Enter your password to continue') . "</p>";

$linkReturnHtml = '<a class="float-center" style="margin-left: 0px" href="' . Url::to(Yii::$app->user->loginUrl) . '">' . Yii::t($moduleId, 'you are not {username}?', ['username' => $model->username]) . '</a>';

$imgProfile = Yii::$app->html::img($this->theme->getImageUrl("user.png"), ['style' => 'width:5rem;']);

?>



<div class="security-expired-session">

    <?php $form = ActiveForm::begin([
        'id' => 'expired-session-form',
    ]); ?>


    <?php
    $formColumnsHtml = Yii::$app->ui->renderFormColumns($form, $modelsFormColums, $this->context, '_session_expired_tpl', $model->isNewRecord ? ConstantsHelper::RENDER_CREATE : ConstantsHelper::RENDER_UPDATE);

    $formColumnsHtml = str_replace('{{title}}', $titleHtml, $formColumnsHtml);
    $formColumnsHtml = str_replace('{{btn-login}}', $btnLoginHtml, $formColumnsHtml);
    $formColumnsHtml = str_replace('{{link-returnlogin}}', $linkReturnHtml, $formColumnsHtml);
    $formColumnsHtml = str_replace('{{img-profile}}', $imgProfile, $formColumnsHtml);

    ?>

    <?= $formColumnsHtml ?>

    <?php ActiveForm::end(); ?>
</div>
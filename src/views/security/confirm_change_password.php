<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model User */
/* @var $form kfit\core\widgets\ActiveForm */
/* @var $model app\models\registerForm */

$this->title = Yii::t($model->module->id, 'Password change request');
?>

<div class="row justify-content-md-center align-items-center align-self-center m-0" style="height: 100%">
    <div class="col col-md-5 align-self-center align-middle">
        <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
                <div class="content-fluid"></div>
                <div class="card my-5 px-5 pb-5">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-12">
                                <em class="fa fa-envelope " style="font-size: 5rem;"></em>
                                <h3><?= $this->title; ?></h3>
                                <p>Se envió un correo electrónico de confirmación a <strong><?= $model->email ?></strong>
                                </p>
                                <p>
                                    Verifique su bandeja de entrada y haga clic en el enlace "confirmar el cambio de la contraseña" para restablecer su contraseña.
                                </p>
                                <div style="margin-top:3rem;">
                                    <a class="float-center" style="margin-left: 0px" href="<?php echo Url::to(['security/login']) ?>"><?= Yii::t($model->module->id, 'Go to the start session page') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
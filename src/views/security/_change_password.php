<?php

use kfit\core\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $form kfit\core\widgets\ActiveForm */
/* @var $model app\models\registerForm */

$this->title = Yii::t($moduleId, 'Restore password');

?>

<div class="row justify-content-center align-items-center align-self-center m-0" style="height: 100%">

    <div class="col-12 col-md-4 col-lg-4 align-self-center align-middle">        <div class="vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="vertical-align-middle">
                <div class="card px-25 pb-20">
                    <div class="card-body">
                        <div>
                            <div class="row">

                                <div class="col-12">
                                    <h3><?= Yii::t($moduleId, 'Enter your new password'); ?></h3>
                                </div>
                            </div>
                        </div>

                        <div class="security-expired-session">

                            <?php $form = ActiveForm::begin([
                                'id' => 'expired-session-form',
                            ]); ?>
                            <div class="row pt-15">
                                <div class="col-12 text-left">
                                    <?= $form->field($model, 'email', ['help' => '', 'popover' => $model->getHelp('email')])
                                        ->textInput([
                                            'autofocus' => true,
                                            'disabled' => true,
                                        ]) ?>
                                </div>
                                <div class="col-12 text-left">
                                    <?= $form->field($model, 'password', ['help' => '', 'popover' => $model->getHelp('password')])->passwordInput() ?>
                                </div>
                                <div class="col-12 text-left">
                                    <?= $form->field($model, 'confirmPassword', ['help' => '', 'popover' => $model->getHelp('confirmPassword')])->passwordInput() ?>
                                </div>

                            </div>

                            <div class="col-12 p-0">
                                <?= Yii::$app->html::submitButton(Yii::t($moduleId, 'Restore password'), ['class' => 'btn btn-primary btn-block btn-md mt-10', 'name' => 'register-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
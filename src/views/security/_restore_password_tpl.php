<?php if (!$this->context->action->isModal) : ?>
    <div class="row justify-content-center align-items-center align-self-center m-0 h-100">

        <div class="col col-md-6 align-self-center align-middle">

            <div class="vertical-align">
                <div class="vertical-align-middle">
                    <div class="card py-10 px-20">
                        <div class="card-body">

                        <?php endif ?>

                        <div class="row">
                            <div class="col-12">
                                {{title}}
                            </div>
                            <div class="col-12 text-left pt-15">
                                {{email}}
                            </div>
                        </div>

                        <div class="row">

                            <div class="container-button col-12">
                                {{btn-restore-password}}
                            </div>

                            <div class="col-lg-12 mt-2 text-center">
                                {{link-return-login}}
                            </div>
                        </div>

                        <?php if (!$this->context->action->isModal) : ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
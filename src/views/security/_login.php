<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\registerForm */

use kfit\core\helpers\ConstantsHelper;
use kfit\core\widgets\ActiveForm;

use yii\web\View;
use yii\helpers\Url;

if (!isset($modelsFormColums)) {
    $modelsFormColums = ['model' => $model];
}
$isModal = $this->context->action->isModal;
$model = $modelsFormColums['model'];

$this->title = Yii::t('adm', 'Login');

?>
<script src="<?= Yii::$app->controller->module->socialAuthConfig['firebaseUrl'] ?>"></script>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
]); ?>

<?php
$formColumnsHtml = Yii::$app->ui->renderFormColumns($form, $modelsFormColums, $this->context, '_login_tpl', $model->isNewRecord ? ConstantsHelper::RENDER_CREATE : ConstantsHelper::RENDER_UPDATE);

$titleHtml = Yii::t('adm', 'Login to the system');

$linkForgotHtml = Yii::$app->html::a(
    Yii::t('adm', 'Forgot your password?'),
    ['security/restore-password'],
    [
        'class' => $isModal ? 'load-modal' : ''
    ]
);
$btnLoginHtml = Yii::$app->html::submitButton(
    Yii::t('adm', 'Log in'),
    [
        'class' => 'btn btn-primary btn-block btn-md mt-10',
        'name' => 'login-button'
    ]
);
$separatorHtml = Yii::$app->html::tag('h5', 'ó', ['class' => 'login-letter-separator']);
$btnGoogleHtml = Yii::$app->html::a(
    Yii::t('adm', 'Log in with Google'),
    '#',
    [
        "class" => "btn btn-google btn-block btn-md",
        "data-socialtype" => "GOP",
        "onclick" => "setLogin(this, event)"
    ]
);
$btnFacebookHtml = Yii::$app->html::a(
    Yii::t('adm', 'Log in with Facebook'),
    '#',
    [
        "class" => "btn btn-facebook btn-block btn-md",
        "data-socialtype" => "FAB",
        "onclick" => "setLogin(this, event)"
    ]
);

$formColumnsHtml = str_replace('{{title}}', $titleHtml, $formColumnsHtml);
$formColumnsHtml = str_replace('{{link-forgotPassword}}', $linkForgotHtml, $formColumnsHtml);
$formColumnsHtml = str_replace('{{btn-login}}', $btnLoginHtml, $formColumnsHtml);
$formColumnsHtml = str_replace('{{separator-login}}', $separatorHtml, $formColumnsHtml);
$formColumnsHtml = str_replace('{{btn-google}}', $btnGoogleHtml, $formColumnsHtml);
$formColumnsHtml = str_replace('{{btn-facebook}}', $btnFacebookHtml, $formColumnsHtml);
?>

<?= $formColumnsHtml; ?>

<?php ActiveForm::end(); ?>
<?php
$urlSocialLogin = Url::to('login');
$variables = Yii::$app->controller->module->socialAuthConfig;
unset($variables['firebaseUrl']);
$variables = json_encode($variables);
$js = <<<JS
    var config = $variables;
    // firebase.initializeApp(config);
    
    function setLogin(btn, event) {
        btn = $(btn);
        let provider = getProvider(btn.attr('data-socialtype'));
        btn.attr('disabled', true);
        firebase.auth().signInWithPopup(provider).then((result) => {
            sendLogin(result, btn.attr('data-socialtype'));
            btn.attr('disabled', false);
        }).catch((error) => {
            console.log(error);
            btn.attr('disabled', false);
        });
        event.preventDefault();
    }

    function getProvider(type) {
        let provider = null;
        switch (type) {
            case 'FAB':
                break;
            case 'GOP': default:
                provider = new firebase.auth.GoogleAuthProvider();
                provider.addScope('email');
                break;
        }
        return provider;
    }

    function sendLogin(data, type) {
        let method = 'post';
        let form = document.createElement('form');
        let attributes = [
            { name: 'username', value: data.additionalUserInfo.profile.name },
            { name: 'email', value: data.additionalUserInfo.profile.email },
            { name: 'name', value: type },
            { name: 'response', value: JSON.stringify(data) },
            { name: 'social_network_user', value: data.additionalUserInfo.profile.id },
            { name: '_csrf', value: $('input[name="_csrf"]').val() }
        ];
        form.setAttribute('method', method);
        form.setAttribute('style', 'display: none;');
        form.setAttribute('action', '$urlSocialLogin');
        for (let attribute of attributes) {
            let hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', attribute.name);
            hiddenField.setAttribute('value', attribute.value);
            form.appendChild(hiddenField);
        }
        document.body.appendChild(form);
        form.submit();
    }
JS;
$this->registerJs(
    $js,
    View::POS_END
); ?>
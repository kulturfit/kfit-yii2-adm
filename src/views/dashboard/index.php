<?php

use yii\helpers\ArrayHelper;

$breadcrumbs = [];
$this->title = Yii::t('adm', 'Rbac');
$this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rbac-default-index">
    <div class="d-flex">
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t('adm', 'Menu'), ['menus/index']) ?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t('adm', 'Users'), ['users/index']) ?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t('adm', 'Roles'), ['roles/index']) ?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t('adm', 'Permissions'), ['permissions/index']) ?>
        </div>
        <div class="col-2">
            <?= Yii::$app->html::a(Yii::t('adm', 'Routes'), ['routes/index']) ?>
        </div>
    </div>
</div>
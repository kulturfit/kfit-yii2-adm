<?php

namespace kfit\adm\modules\api\models\searchs;

use kfit\adm\modules\api\models\base\MenuItems as MenuItemsModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Esta clase representa las búsqueda para el modelo `kfit\adm\models\base\MenuItems`.
 *
 * @package kfit/adm
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class MenuItems extends MenuItemsModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['menu_item_id', 'menu_id', 'parent_menu_id', 'order', 'created_by', 'updated_by'], 'integer'],
            [['name', 'internal', 'route_id', 'link', 'icon', 'target', 'params', 'description', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MenuItemsModel::find();
        $query->alias('mih');
        $query->select([
            'mih.name',
            "CASE WHEN mih.parent_menu_id IS NULL THEN concat(mih.order::text, '0','0')
                ELSE concat(mip.order::text, '1',mih.order::text)
            END as \"order\"",
            'mih.parent_menu_id',
            'mih.menu_item_id',
        ]);
        $query->leftJoin('menu_items mip', 'mih.parent_menu_id = mip.menu_item_id');

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'order' => SORT_ASC,
                ],
                'attributes' => [
                    'order'
        ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'mih.menu_item_id' => $this->menu_item_id,
            'mih.menu_id' => $this->menu_id,
            'mih.parent_menu_id' => $this->parent_menu_id,
            'mih.order' => $this->order,
            'mih.created_by' => $this->created_by,
            'DATE(mih.created_at)' => $this->created_at,
            'mih.updated_by' => $this->updated_by,
            'DATE(mih.updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'mih.name', $this->name])
            ->andFilterWhere(['like', 'mih.internal', $this->internal])
            ->andFilterWhere(['like', 'mih.route_id', $this->route_id])
            ->andFilterWhere(['like', 'mih.link', $this->link])
            ->andFilterWhere(['like', 'mih.icon', $this->icon])
            ->andFilterWhere(['like', 'mih.target', $this->target])
            ->andFilterWhere(['like', 'mih.params', $this->params])
            ->andFilterWhere(['like', 'mih.description', $this->description])
            ->andFilterWhere(['like', 'mih.active', $this->active]);

        return $dataProvider;
    }
}

<?php

namespace kfit\adm\modules\api\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\base\AuthItemChild as AuthItemChildModel;

/**
 * Esta clase representa las búsqueda para el modelo `app\models\base\AuthItemChild`.
 *
 * @package kfit/adm
 * @subpackage models/searchs
 * @category Models
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S. 

 */
class AuthItemChild extends AuthItemChildModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['auth_item_child_id', 'created_by', 'updated_by'], 'integer'],
            [['parent', 'child', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuthItemChildModel::find();

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'auth_item_child_id' => $this->auth_item_child_id,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'parent', $this->parent])
            ->andFilterWhere(['like', 'child', $this->child])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}

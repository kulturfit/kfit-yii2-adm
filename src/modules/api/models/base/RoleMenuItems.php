<?php

namespace kfit\adm\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "role_menu_items".
 * This entity stores all the active menus for a role.
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $role_menu_item_id Identifier of a menu item associated with a role.
 * @property string $role_id Role identifier
 * @property integer $menu_item_id Menu item identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $role Datos relacionados con modelo "AuthItem"
 * @property MenuItems $menuItem Datos relacionados con modelo "MenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class RoleMenuItems extends \kfit\adm\models\base\RoleMenuItems
{ }

<?php

namespace kfit\adm\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "social_networks".
 *
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $social_network_id Social network identifier
 * @property integer $user_id User identifier
 * @property string $social_network_user User identifier at social network
 * @property string $name Abbreviation of the name of the social network FAB -> Facebook; GOP -> Google Plus.
 * @property string $response Information provided by the social network
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $update_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property Users $user Datos relacionados con modelo "Users"
 *
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class SocialNetworks extends \kfit\adm\models\base\SocialNetworks
{ }

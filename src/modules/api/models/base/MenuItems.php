<?php

namespace kfit\adm\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "menu_items".
 * This entity stores all the menu items
 *
 * @package kfit/adm
 * @subpackage models/base
 * @category models
 *
 * @property integer $menu_item_id Identifier menu item
 * @property integer $menu_id
 * @property string $name Menu name
 * @property integer $parent_menu_id Parent menu identifier
 * @property string $internal Define if item is internal or external. Format Y-> Yes  N-> No
 * @property string $route_id Route identifier
 * @property string $link Link to the one that redirects the menu.
 * @property string $icon Icon associated to the menu item
 * @property integer $order Order in which the menu item is displayed
 * @property string $target Define where the content of the menu item will be displayed, format: SELF = En la misma pestaña BLANK = En una pestaña nueva
 * @property string $params Item parameters
 * @property string $description Description of the menu item
 * @property string $active Define if it's active or not. Format : Y-> Yes N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $route Datos relacionados con modelo "AuthItem"
 * @property MenuItems $parentMenu Datos relacionados con modelo "MenuItems"
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 * @property Menus $menu Datos relacionados con modelo "Menus"
 * @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class MenuItems extends \kfit\adm\models\base\MenuItems
{ }

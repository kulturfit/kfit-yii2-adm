<?php

namespace kfit\adm\modules\api\controllers;

use kfit\core\rest\ActiveController;
use Yii;
use kfit\core\rest\Controller;

/**
 * Description of SyncController
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
class SyncController extends Controller
{

    public $defaultAction = 'verify';

    /**
     * Retorna la lista de behaviors que el controlador implementa en el rest api
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        return $behaviors;
    }

    /**
     * Login de usuario
     * @return type
     * @throws \yii\web\HttpException
     */
    public function actionVerify()
    {
        $controllerList = [];
        if ($handle = opendir(Yii::getAlias("@kfit/adm/modules/api/controllers"))) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && substr($file, strrpos($file, '.') - 10) == 'Controller.php') {
                    $controllerList[] = $file;
                }
            }
            closedir($handle);
        }
        asort($controllerList);

        $finalControllerList = [];

        foreach ($controllerList as $key => $controllerName) :
            $controllerName = str_replace('.php', '', $controllerName);
            if ($controllerName == 'UserController') {
                continue;
            }
            try {
                $controller = Yii::createObject("kfit\adm\modules\api\controllers\\{$controllerName}", ['controller', 'sync']);
                if ($controller instanceof ActiveController) {

                    $lastDate = Yii::$app->request->post('last_date', null);

                    $model = Yii::createObject($controller->searchModel);
                    $model->lastDate = $lastDate;

                    $finalControllerList[] = [
                        'controller' => $controllerName,
                        'model' => $model::tableName(),
                        'sync' => $model->requireSync(),
                    ];
                }
            } catch (\Exception $ex) { }
        endforeach;

        return $this->serializeData($finalControllerList);
    }
}

<?php

namespace kfit\adm\modules\api\controllers\actions;

use kfit\adm\modules\api\controllers\actions\BaseAction;

use Yii;
use kfit\core\components\DynamicModel;

/**
 * Acción VerifyTokenAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class LogoutAction extends BaseAction
{
    /**
     * Permite ejecutar la acción para validar un token
     *
     * @return mixed
     */
    public function run()
    {

        $userObject = Yii::$app->user->identity;
        $userObject->auth_key = Yii::$app->security->generateRandomString();
        $userObject->update(false, ['auth_key']);

        return [
            'message' => Yii::t('adm', $this->messageOnSuccess)
        ];
    }
}

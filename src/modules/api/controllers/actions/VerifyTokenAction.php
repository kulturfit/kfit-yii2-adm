<?php

namespace kfit\adm\modules\api\controllers\actions;

use kfit\adm\modules\api\controllers\actions\BaseAction;

use Yii;
use kfit\core\components\DynamicModel;

/**
 * Acción VerifyTokenAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class VerifyTokenAction extends BaseAction
{
    public $modelValidate;
    public $modelUser;

    /**
     * Permite ejecutar la acción para validar un token
     *
     * @return mixed
     */
    public function run()
    {
        return Yii::createObject(\kfit\adm\modules\api\models\searchs\Users::class)::findOne(Yii::$app->user->identity->id);
    }
}

<?php

namespace kfit\adm\modules\api\controllers\actions;

use Yii;
use kfit\adm\modules\api\controllers\actions\BaseAction;
use kfit\core\components\DynamicModel;
use kfit\core\helpers\ArrayHelper;
use kfit\adm\models\base\SocialNetworks;
use yii\web\UnauthorizedHttpException;
use kfit\adm\Module;

/**
 * Acción para la gestión del inicio de sesión por medio de redes sociales
 *
 * @package kfit\adm\modules\api\controllers\actions
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 1.0.0
 */
class LoginSocialAction extends BaseAction
{
    public $identityClass;

    /**
     * Manejo de la acción
     *
     * @return mixed
     */
    public function run()
    {
        $user = null;
        $request = Yii::$app->request;
        $modelInstance = static::getModelInstance();
        if ($modelInstance->load($request->post(), '')) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($modelInstance->validate()) {
                $user = $this->identityClass::findByName($modelInstance->email);
                if (!$user) {
                    $user = static::createUser($modelInstance);
                }
                if (static::validateSocialNetwork($modelInstance, $user)) {
                    $user->auth_key = Yii::$app->security->generateRandomString(32);
                    $user->save(false);
                } else {
                    throw new UnauthorizedHttpException(Yii::t($user->module->id, 'Your request was made with invalid credentials.'));
                }
            } else {
                Yii::$app->response->setStatusCode(400, Yii::t(Module::getInstance()->id, 'There are errors in the data sent.'));
                return $modelInstance->getErrors();
            }
        }
        return $user;
    }

    /**
     * Retorna la instancia del modelo dinámico
     *
     * @return \kfit\core\components\DynamicModel
     */
    public static function getModelInstance()
    {
        return DynamicModel::withRules(
            [
                'username',
                'email',
                'type',
                'uid',
                'response'
            ],
            [
                [
                    [
                        'username',
                        'type',
                        'uid',
                        'response',
                        'email'
                    ],
                    'required'
                ],
                ['uid', 'string', 'max' => 128],
                [
                    'type',
                    'in',
                    'allowArray' => true,
                    'range' => ['FAB', 'GOP']
                ]
            ]
        );
    }

    /**
     * Realiza la creación del usuario en la base de datos
     * 
     * @param \kfit\core\components\DynamicModel $model
     * @return Users
     */
    public static function createUser($model)
    {
        dd('No mames');
        $user = Yii::createObject(Yii::$app->user->identityClass);
        $user->load(ArrayHelper::merge($model->getAttributes(), [
            'confirmed_email' => 'Y',
            'password' => Yii::$app->security->generateRandomKey(12),
            'blocked' => 'N'
        ]), '');
        if ($user->save(false)) {
            $socialNetwork = new SocialNetworks;
            $socialNetwork->load([
                'social_network_user' => $model->uid,
                'name' => $model->type,
                'response' => $model->response,
                'user_id' => $user->primaryKey
            ], '');
            $socialNetwork->save(false);
        }
        return $user;
    }

    /**
     * Realiza la validación de la red social
     * 
     * @param DynamicModel $model
     * @param Users
     * @return boolean
     */
    public static function validateSocialNetwork($model, $user)
    {
        $finded = false;
        $socialNetworks = $user->socialNetworks;
        if ($socialNetworks) {
            foreach ($socialNetworks as $social) {
                if ($social->social_network_user == $model->uid && $social->name == $model->type) {
                    $finded = true;
                    break;
                }
            }
        }
        return $finded;
    }
}

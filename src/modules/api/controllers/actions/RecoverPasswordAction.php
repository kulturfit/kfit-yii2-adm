<?php

namespace kfit\adm\modules\api\controllers\actions;

use kfit\adm\modules\api\controllers\actions\BaseAction;

use Yii;
use kfit\core\components\DynamicModel;

/**
 * Acción RecoverPasswordAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class RecoverPasswordAction extends BaseAction
{
    public $modelValidate;
    public $modelUser;

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'We have sent a message to your email with a link to confirm your password change request.';

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnError = 'Operation failed';


    /**
     * Permite ejecutar la acción para validar un token
     *
     * @return mixed
     */
    public function run()
    {
        $modelClass = Yii::$container->get(Yii::$app->user->identityClass);
        $this->modelValidate = DynamicModel::withRules([
            'email' => Yii::$app->request->post('email')
        ], [
            [['email'], 'required'],
            [['email'], 'match', 'pattern' => '/^[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,3}/i'],
            [['email'], 'string', 'max' => 255],
            [
                'email',
                'exist',
                'targetClass' => $modelClass,
                'targetAttribute' => ['email' => 'email'],
                'filter' => function ($query) {
                    $modelClass = Yii::$container->get(Yii::$app->user->identityClass);
                    $query->andWhere([$modelClass::STATUS_COLUMN => $modelClass::STATUS_ACTIVE]);
                }
            ]
        ]);

        if ($this->modelValidate->validate()) {
            $this->modelUser = $modelClass->findByName($this->modelValidate->email);
            $this->modelUser->sendRestorePasswordEmail();
            $result =  [
                'message' => Yii::t('adm', $this->messageOnSuccess)
            ];
        } else {
            $result = $this->modelValidate;
        }

        return $result;
    }
}

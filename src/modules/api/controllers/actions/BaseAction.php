<?php

namespace kfit\adm\modules\api\controllers\actions;

use kfit\adm\components\AdmAssets;
use kfit\adm\Module;
use Yii;

/**
 * Acción BaseAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class BaseAction extends \kfit\core\actions\BaseAction
{

    /**
     * This method is called right before `run()` is executed.
     * You may override this method to do preparation work for the action run.
     * If the method returns false, it will cancel the action.
     *
     * @return bool whether to run the action.
     */
    protected function beforeRun()
    {
        $this->module = (Module::getInstance()) ?? Yii::$app;
        return true;
    }
}

<?php

namespace kfit\adm\modules\api\controllers;

use Yii;
use kfit\core\filters\auth\HttpSocialAuth;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use kfit\adm\modules\api\controllers\actions\LoginAction;
use kfit\adm\modules\api\controllers\actions\RegisterAction;
use kfit\adm\modules\api\controllers\actions\VerifyTokenAction;
use kfit\adm\modules\api\controllers\actions\LogoutAction;
use kfit\adm\modules\api\controllers\actions\RecoverPasswordAction;
use kfit\adm\modules\api\controllers\behaviors\LanguageBehavior;
use kfit\adm\modules\api\controllers\actions\LoginSocialAction;

/**
 *
 * Implementación de todas las funcionalidad de seguridad para el API.
 *
 * @package app
 * @subpackage modules\api\controllers
 * @category controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class SecurityController extends \kfit\core\rest\Controller
{
    const ACTION_LOGIN = 'login';
    const ACTION_LOGOUT = 'logout';
    const ACTION_REGISTER = 'register';
    const ACTION_VERIFY_TOKEN = 'verify-token';
    const ACTION_RECOVER_ACCOUNT = 'recover-password';
    const ACTION_LOGIN_SOCIAL = 'login-social';

    public $defaultAction = self::ACTION_LOGIN;

    /**
     * Undocumented function
     *
     * @return void
     */
    public function actions()
    {
        return [
            self::ACTION_LOGIN => LoginAction::class,
            self::ACTION_REGISTER => RegisterAction::class,
            self::ACTION_VERIFY_TOKEN => VerifyTokenAction::class,
            self::ACTION_LOGOUT => LogoutAction::class,
            self::ACTION_RECOVER_ACCOUNT => RecoverPasswordAction::class,
            self::ACTION_LOGIN_SOCIAL => [
                'class' => LoginSocialAction::class,
                'identityClass' => Yii::$app->user->identityClass
            ]
        ];
    }

    /**
     * Retorna la lista de behaviors que el controlador implementa
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator'], $behaviors['verbFilter']);

        $behaviors['language'] = LanguageBehavior::class;

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className()
            ],
            'only' => [
                self::ACTION_LOGOUT,
                self::ACTION_VERIFY_TOKEN,
                self::ACTION_LOGOUT,
            ],
            'optional' => [
                self::ACTION_LOGIN
            ]
        ];

        $behaviors['verbFilter'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                self::ACTION_LOGIN  => ['POST'],
                self::ACTION_REGISTER  => ['POST'],
                self::ACTION_VERIFY_TOKEN => ['GET'],
                self::ACTION_LOGOUT => ['POST'],
                self::ACTION_RECOVER_ACCOUNT => ['POST'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Validación de señal
     */
    public function actionValidateConection()
    {
        return [
            'message' => Yii::t('adm', 'Online')
        ];
    }
}

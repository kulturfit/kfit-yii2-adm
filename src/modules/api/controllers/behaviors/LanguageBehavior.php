<?php

namespace kfit\adm\modules\api\controllers\behaviors;

use Yii;

/**
 * Acción BaseAction.
 *
 * @package kfit/adm
*
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class LanguageBehavior extends \yii\base\Behavior
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function events()
    {
        return [
            \yii\base\Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function beforeAction($event)
    {
        if (Yii::$app->request->isGet) {
            $language = Yii::$app->request->get('language', Yii::$app->language);
        } else if (Yii::$app->request->isPost) {
            $language = Yii::$app->request->post('language', Yii::$app->language);
        }
        Yii::$app->language = $language;
        return true;
    }
}

<?php

namespace kfit\adm\modules\redis\controllers;

use kfit\core\base\Controller;

/**
* UsersController Clase encargada de presentar y manipular la información del modelo Users para las solicitudes en redis
*
* @package kfit\adm\modules\redis\controllers 
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class UsersController extends Controller
{

    public $isModal = true;

    /**
    * @var Model $modelClass Modelo para las operaciones CRUD
    */
    public $modelClass = \kfit\adm\modules\redis\models\app\Users::class;

    /**
    * @var Model $searchModel Modelo para las búsquedas
    */
    public $searchModelClass = \kfit\adm\modules\redis\models\searchs\Users::class;


}
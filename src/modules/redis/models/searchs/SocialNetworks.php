<?php

namespace kfit\adm\modules\redis\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\adm\modules\redis\models\app\SocialNetworks as SocialNetworksModel;

/**
 * SocialNetworks represents the model behind the search form about `kfit\adm\models\app\SocialNetworks`.
 */
class SocialNetworks extends SocialNetworksModel
{
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\searchs\SocialNetworks::class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $defaultRules = [
            ['transaction', 'safe']
        ];
        $modelRules = [
            [['social_network_id', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['social_network_user', 'name', 'response', 'active', 'created_at', 'updated_at', 'lastDate', 'notIn'], 'safe'],
        ];
        return Yii::$app->arrayHelper::merge($defaultRules, $modelRules);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['transaction' => $this->transaction]);

        $query->andFilterWhere([
            'social_network_id' => $this->social_network_id,
            'user_id' => $this->user_id,
            'social_network_user' => $this->social_network_user,
            'name' => $this->name,
            'response' => $this->response,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
    
}

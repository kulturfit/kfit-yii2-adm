<?php

namespace kfit\adm\modules\redis\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\adm\modules\redis\models\app\MenuItems as MenuItemsModel;

/**
 * MenuItems represents the model behind the search form about `kfit\adm\models\app\MenuItems`.
 */
class MenuItems extends MenuItemsModel
{
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\searchs\MenuItems::class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $defaultRules = [
            ['transaction', 'safe']
        ];
        $modelRules = [
            [['menu_item_id', 'menu_id', 'parent_menu_id', 'order', 'created_by', 'updated_by'], 'integer'],
            [['name', 'internal', 'route_id', 'link', 'icon', 'target', 'params', 'description', 'active', 'created_at', 'updated_at', 'lastDate', 'notIn'], 'safe'],
        ];
        return Yii::$app->arrayHelper::merge($defaultRules, $modelRules);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['transaction' => $this->transaction]);

        $query->andFilterWhere([
            'menu_item_id' => $this->menu_item_id,
            'menu_id' => $this->menu_id,
            'name' => $this->name,
            'parent_menu_id' => $this->parent_menu_id,
            'internal' => $this->internal,
            'route_id' => $this->route_id,
            'link' => $this->link,
            'icon' => $this->icon,
            'order' => $this->order,
            'target' => $this->target,
            'params' => $this->params,
            'description' => $this->description,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
    
}

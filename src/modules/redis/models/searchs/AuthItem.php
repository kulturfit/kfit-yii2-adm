<?php

namespace kfit\adm\modules\redis\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\adm\modules\redis\models\app\AuthItem as AuthItemModel;

/**
 * AuthItem represents the model behind the search form about `kfit\adm\models\app\AuthItem`.
 */
class AuthItem extends AuthItemModel
{
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\searchs\AuthItem::class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $defaultRules = [
            ['transaction', 'safe']
        ];
        $modelRules = [
            [['name', 'type', 'description', 'rule_name', 'data', 'active', 'created_at', 'updated_at', 'lastDate', 'notIn'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
        ];
        return Yii::$app->arrayHelper::merge($defaultRules, $modelRules);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['transaction' => $this->transaction]);

        $query->andFilterWhere([
            'name' => $this->name,
            'type' => $this->type,
            'description' => $this->description,
            'rule_name' => $this->rule_name,
            'data' => $this->data,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
    
}

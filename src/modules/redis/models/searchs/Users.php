<?php

namespace kfit\adm\modules\redis\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\adm\modules\redis\models\app\Users as UsersModel;

/**
 * Users represents the model behind the search form about `kfit\adm\models\app\Users`.
 */
class Users extends UsersModel
{
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\searchs\Users::class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $defaultRules = [
            ['transaction', 'safe']
        ];
        $modelRules = [
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['username', 'email', 'password_hash', 'auth_key', 'password_reset_token', 'email_key', 'confirmed_email', 'blocked', 'accepted_terms', 'active', 'created_at', 'updated_at', 'lastDate', 'notIn'], 'safe'],
        ];
        return Yii::$app->arrayHelper::merge($defaultRules, $modelRules);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['transaction' => $this->transaction]);

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'username' => $this->username,
            'email' => $this->email,
            'password_hash' => $this->password_hash,
            'auth_key' => $this->auth_key,
            'password_reset_token' => $this->password_reset_token,
            'email_key' => $this->email_key,
            'confirmed_email' => $this->confirmed_email,
            'blocked' => $this->blocked,
            'accepted_terms' => $this->accepted_terms,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
    
}

<?php

namespace kfit\adm\modules\redis\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\adm\modules\redis\models\app\AuthItemChild as AuthItemChildModel;

/**
 * AuthItemChild represents the model behind the search form about `kfit\adm\models\app\AuthItemChild`.
 */
class AuthItemChild extends AuthItemChildModel
{
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\searchs\AuthItemChild::class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $defaultRules = [
            ['transaction', 'safe']
        ];
        $modelRules = [
            [['auth_item_child_id', 'created_by', 'updated_by'], 'integer'],
            [['parent', 'child', 'active', 'created_at', 'updated_at', 'lastDate', 'notIn'], 'safe'],
        ];
        return Yii::$app->arrayHelper::merge($defaultRules, $modelRules);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['transaction' => $this->transaction]);

        $query->andFilterWhere([
            'auth_item_child_id' => $this->auth_item_child_id,
            'parent' => $this->parent,
            'child' => $this->child,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
    
}

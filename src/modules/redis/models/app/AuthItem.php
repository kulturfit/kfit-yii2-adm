<?php

namespace kfit\adm\modules\redis\models\app;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* AuthItem 
* 
* @package kfit\adm\modules\redis\models\app 
*
* @property string $name Define the name for auth item (roles,routes and permissions).
* @property string $type Define item type and  take the next values:  ROL -> Roles  PER -> Permissions  ROU -> Routes
* @property string $description Item description
* @property string $rule_name Identifier name of auth rule
* @property string $data Additional value that the item takes
* @property string $active Define if it's active or not. Format : Y->Yes, N->No
* @property integer $created_by It's the identifier of the user who created the record.
* @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH:MM:SS
* @property integer $updated_by It's the identifier of the user who updated the record.
* @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH:MM:SS
* @property AuthAssignment[] $authAssignments Datos relacionados con modelo "AuthAssignment"
* @property AuthRule $ruleName Datos relacionados con modelo "AuthRule"
* @property AuthItemChild[] $authItemChildren Datos relacionados con modelo "AuthItemChild"
* @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
* @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class AuthItem extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\app\AuthItem::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
                [['rule_name'], 'default', 'value'=>'0']             
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

        /**
     * Definición de la relación con el modelo "AuthAssignments".
     *
     * @return \kfit\adm\models\app\AuthAssignments
     */
    public function getAuthAssignments()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\AuthAssignment::class), ['item_name' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "RuleName".
     *
     * @return \kfit\adm\models\app\RuleName
     */
    public function getRuleName()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\AuthRule::class), ['name' => 'rule_name'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "AuthItemChildren".
     *
     * @return \kfit\adm\models\app\AuthItemChildren
     */
    public function getAuthItemChildren()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\AuthItemChild::class), ['child' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "MenuItems".
     *
     * @return \kfit\adm\models\app\MenuItems
     */
    public function getMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\MenuItems::class), ['route_id' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "RoleMenuItems".
     *
     * @return \kfit\adm\models\app\RoleMenuItems
     */
    public function getRoleMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\RoleMenuItems::class), ['role_id' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }


    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/redis/auth-items',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}

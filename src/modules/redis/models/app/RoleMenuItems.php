<?php

namespace kfit\adm\modules\redis\models\app;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* RoleMenuItems 
* 
* @package kfit\adm\modules\redis\models\app 
*
* @property integer $role_menu_item_id Identifier of a menu item associated with a role.
* @property string $role_id Role identifier
* @property integer $menu_item_id Menu item identifier
* @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
* @property integer $created_by It's the identifier of the user who created the record.
* @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
* @property integer $updated_by It's the identifier of the user who updated the record.
* @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
* @property AuthItem $role Datos relacionados con modelo "AuthItem"
* @property MenuItems $menuItem Datos relacionados con modelo "MenuItems"
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class RoleMenuItems extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\app\RoleMenuItems::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
                [['role_id', 'menu_item_id'], 'default', 'value'=>'0']             
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

        /**
     * Definición de la relación con el modelo "Role".
     *
     * @return \kfit\adm\models\app\Role
     */
    public function getRole()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\AuthItem::class), ['name' => 'role_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "MenuItem".
     *
     * @return \kfit\adm\models\app\MenuItem
     */
    public function getMenuItem()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\MenuItems::class), ['menu_item_id' => 'menu_item_id'])->cache(3);
    }


    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/redis/role-menu-items',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}

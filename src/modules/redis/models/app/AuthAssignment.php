<?php

namespace kfit\adm\modules\redis\models\app;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* AuthAssignment 
* 
* @package kfit\adm\modules\redis\models\app 
*
* @property integer $auth_assignment_id Assignment identifier
* @property string $item_name Role identifier
* @property integer $user_id Identifier user
* @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
* @property integer $created_by It's the identifier of the user who created the record.
* @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
* @property integer $updated_by It's the identifier of the user who updated the record.
* @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
* @property AuthItem $itemName Datos relacionados con modelo "AuthItem"
* @property Users $user Datos relacionados con modelo "Users"
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class AuthAssignment extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\app\AuthAssignment::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
                [['item_name', 'user_id'], 'default', 'value'=>'0']             
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

        /**
     * Definición de la relación con el modelo "ItemName".
     *
     * @return \kfit\adm\models\app\ItemName
     */
    public function getItemName()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\AuthItem::class), ['name' => 'item_name'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "User".
     *
     * @return \kfit\adm\models\app\User
     */
    public function getUser()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\Users::class), ['user_id' => 'user_id'])->cache(3);
    }


    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/redis/auth-assignments',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}

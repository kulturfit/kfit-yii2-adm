<?php

namespace kfit\adm\modules\redis\models\app;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* AuthItemChild 
* 
* @package kfit\adm\modules\redis\models\app 
*
* @property integer $auth_item_child_id Auth item child identifier
* @property string $parent Item parent identifier
* @property string $child Item child identifier
* @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
* @property integer $created_by  It's the identifier of the user who created the record.
* @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
* @property integer $updated_by It's the identifier of the user who updated the record.
* @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
* @property AuthItem $child0 Datos relacionados con modelo "AuthItem"
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class AuthItemChild extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\app\AuthItemChild::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
                [['child'], 'default', 'value'=>'0']             
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

        /**
     * Definición de la relación con el modelo "Child0".
     *
     * @return \kfit\adm\models\app\Child0
     */
    public function getChild0()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\AuthItem::class), ['name' => 'child'])->cache(3);
    }


    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/redis/auth-item-children',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}

<?php

namespace kfit\adm\modules\redis\models\app;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* Users 
* 
* @package kfit\adm\modules\redis\models\app 
*
* @property integer $user_id User identifier
* @property string $username Username with which the user logs in.
* @property string $email User email
* @property string $password_hash Password encrypted.
* @property string $auth_key Random code that identifies the access key for resources by the api rest
* @property string $password_reset_token Random code to identify when to reset the password   
* @property string $email_key Key to validate that the email is valid
* @property string $confirmed_email Establishes if the email is confirmed. Format  Y -> Yes  N -> No
* @property string $blocked Establishes if the user is blocked  Y -> Yes  N -> No
* @property string $accepted_terms It determines if the user accepts or not the terms and conditions.  Format: Y->Yes or N->No
* @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
* @property integer $created_by It's the identifier of the user who created the record.
* @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
* @property integer $updated_by  It's the identifier of the user who updated the record.
* @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
* @property Actors[] $actors Datos relacionados con modelo "Actors"
* @property AuthAssignment[] $authAssignments Datos relacionados con modelo "AuthAssignment"
* @property SocialNetworks[] $socialNetworks Datos relacionados con modelo "SocialNetworks"
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class Users extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\app\Users::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
             
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

        /**
     * Definición de la relación con el modelo "Actors".
     *
     * @return \kfit\adm\models\app\Actors
     */
    public function getActors()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\Actors::class), ['user_id' => 'user_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "AuthAssignments".
     *
     * @return \kfit\adm\models\app\AuthAssignments
     */
    public function getAuthAssignments()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\AuthAssignment::class), ['user_id' => 'user_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "SocialNetworks".
     *
     * @return \kfit\adm\models\app\SocialNetworks
     */
    public function getSocialNetworks()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\SocialNetworks::class), ['user_id' => 'user_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }


    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/redis/users',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}

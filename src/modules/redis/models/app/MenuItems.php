<?php

namespace kfit\adm\modules\redis\models\app;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* MenuItems 
* 
* @package kfit\adm\modules\redis\models\app 
*
* @property integer $menu_item_id Identifier menu item
* @property integer $menu_id 
* @property string $name Menu name
* @property integer $parent_menu_id Parent menu identifier
* @property string $internal Define if item is internal or external. Format Y-> Yes  N-> No
* @property string $route_id Route identifier
* @property string $link Link to the one that redirects the menu.
* @property string $icon Icon associated to the menu item
* @property integer $order Order in which the menu item is displayed
* @property string $target Define where the content of the menu item will be displayed, format:  SELF = En la misma pestaña  BLANK = En una pestaña nueva
* @property string $params Item parameters
* @property string $description Description of the menu item
* @property string $active Define if it's active or not. Format :  Y-> Yes  N -> No
* @property integer $created_by It's the identifier of the user who created the record.
* @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
* @property integer $updated_by It's the identifier of the user who updated the record.
* @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
* @property AuthItem $route Datos relacionados con modelo "AuthItem"
* @property MenuItems $parentMenu Datos relacionados con modelo "MenuItems"
* @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
* @property Menus $menu Datos relacionados con modelo "Menus"
* @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class MenuItems extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\app\MenuItems::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
                [['route_id', 'parent_menu_id', 'menu_id'], 'default', 'value'=>'0']             
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

        /**
     * Definición de la relación con el modelo "Route".
     *
     * @return \kfit\adm\models\app\Route
     */
    public function getRoute()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\AuthItem::class), ['name' => 'route_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "ParentMenu".
     *
     * @return \kfit\adm\models\app\ParentMenu
     */
    public function getParentMenu()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\MenuItems::class), ['menu_item_id' => 'parent_menu_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "MenuItems".
     *
     * @return \kfit\adm\models\app\MenuItems
     */
    public function getMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\MenuItems::class), ['parent_menu_id' => 'menu_item_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "Menu".
     *
     * @return \kfit\adm\models\app\Menu
     */
    public function getMenu()
    {
        return $this->hasOne(Yii::$container->get(\kfit\adm\models\app\Menus::class), ['menu_id' => 'menu_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "RoleMenuItems".
     *
     * @return \kfit\adm\models\app\RoleMenuItems
     */
    public function getRoleMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\RoleMenuItems::class), ['menu_item_id' => 'menu_item_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }


    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/redis/menu-items',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}

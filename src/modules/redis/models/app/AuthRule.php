<?php

namespace kfit\adm\modules\redis\models\app;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* AuthRule 
* 
* @package kfit\adm\modules\redis\models\app 
*
* @property string $name Rule identifier
* @property string $data Additional value of the rule identifier
* @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
* @property integer $created_by It's the identifier of the user who created the record.
* @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
* @property integer $updated_by It's the identifier of the user who updated the record.
* @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
* @property AuthItem[] $authItems Datos relacionados con modelo "AuthItem"
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class AuthRule extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \kfit\adm\models\app\AuthRule::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
             
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

        /**
     * Definición de la relación con el modelo "AuthItems".
     *
     * @return \kfit\adm\models\app\AuthItems
     */
    public function getAuthItems()
    {
        $query = $this->hasMany(Yii::$container->get(\kfit\adm\models\app\AuthItem::class), ['rule_name' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }


    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/redis/auth-rules',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}

<?php

namespace kfit\adm\modules\redis;

use Yii;

use yii\base\Application;
use yii\base\BootstrapInterface;
use kfit\adm\modules\redis\models\base\AuthAssignment;
use kfit\adm\modules\redis\models\base\AuthItem;
use kfit\adm\modules\redis\models\base\AuthItemChild;
use kfit\adm\modules\redis\models\base\AuthRule;
use kfit\adm\modules\redis\models\base\MenuItems;
use kfit\adm\modules\redis\models\base\Menus;
use kfit\adm\modules\redis\models\base\RoleMenuItems;
use kfit\adm\modules\redis\models\base\SocialNetworks;
use kfit\adm\modules\redis\models\base\Users;

use kfit\adm\modules\redis\models\searchs\AuthAssignment as AuthAssignmentSearch;
use kfit\adm\modules\redis\models\searchs\AuthItem as AuthItemSearch;
use kfit\adm\modules\redis\models\searchs\AuthItemChild as AuthItemChildSearch;
use kfit\adm\modules\redis\models\searchs\MenuItems as MenuItemsSearch;
use kfit\adm\modules\redis\models\searchs\Menus as MenusSearch;
use kfit\adm\modules\redis\models\searchs\Users as UsersSearch;

/**
 * Class Bootstrap
 * @package kfit\adm\modules\redis\Bootstrap
 * 
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @var array
     */
    private $_modelMap = [
        'base' => [
            'AuthAssignment' => AuthAssignment::class,
            'AuthItem' => AuthItem::class,
            'AuthItemChild' => AuthItemChild::class,
            'AuthRule' => AuthRule::class,
            'MenuItems' => MenuItems::class,
            'Menus' => Menus::class,
            'RoleMenuItems' => RoleMenuItems::class,
            'SocialNetworks' => SocialNetworks::class,
            'Users' => Users::class,
        ],
        'searchs' => [
            'AuthAssignment' => AuthAssignmentSearch::class,
            'AuthItem' => AuthItemSearch::class,
            'AuthItemChild' => AuthItemChildSearch::class,
            'MenuItems' => MenuItemsSearch::class,
            'Menus' => MenusSearch::class,
            'Users' => UsersSearch::class,
        ]
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('redis')) {
            $app->setModule('redis', 'kfit\adm\modules\redis\Module');
        }
        $this->overrideModels($app, 'adm');
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideModels($app, $moduleId)
    {
        /**
         * @var Module $module
         * @var ActiveRecord $modelName
         */
        if ($app->hasModule($moduleId) && ($module = $app->getModule($moduleId)) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $pathModel => $modelMap) {
                foreach ($modelMap as $name => $definition) {
                    $class = "kfit\\adm\\modules\\{$moduleId}\\models\\{$pathModel}\\" . $name;
                    Yii::$container->set($class, $definition);
                    $modelName = is_array($definition) ? $definition['class'] : $definition;
                    $module->modelMap[$name] = $modelName;
                }
            }
        }
    }
}

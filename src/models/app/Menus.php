<?php

namespace kfit\adm\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "menus".
 * 
 *
 * @package kfit\adm\models\app 
 *
 * @property integer $menu_id Menu identifier
 * @property string $name Menu name
 * @property string $description Menu's description
 * @property string $type Define the menu type:    B-> Backend  F-> Frontend  M -> Movil
 * @property string $position Define the location of the menu
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Menus extends \kfit\adm\models\base\Menus
{

    const BACKEND = 'B';
    const FRONTEND = 'F';
    const MOBILE = 'M';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [
            ['type', 'required']
        ];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $parentFormColumns = parent::formColumns();
        $parentFormColumns['description']['render'] = ['C', 'U'];
        $formColumns = [
            'type' => [
                'widget' => [
                    'class' => \kartik\widgets\Select2::class,
                    'data' => static::getTypes(),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty(),
                    ]
                ]
            ]
        ];
        return Yii::$app->arrayHelper::merge($parentFormColumns, $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $parentGridColumns = parent::gridColumns();
        $parentGridColumns['position']['visible'] = false;
        $gridColumns = [
            'type' => [
                'value' => function ($model) {
                    return static::getTypes($model->type);
                },
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => static::getTypes(),
                'filterInputOptions' => [
                    'id' => Yii::$app->html::activeInputId($this, 'type', null, '-grid'),
                    'placeholder' => Yii::$app->strings::getTextAll()
                ]
            ]
        ];
        return Yii::$app->arrayHelper::merge($parentGridColumns, $gridColumns);
    }

    /**
     * Permite obtener los items del menú de acuerdo al rol de usuario
     * 
     * @return array | $resultItems: listado de items del menú
     */
    public function getItems()
    {

        $sql = "SELECT mih.name, (CASE WHEN mih.parent_menu_id IS NULL
                THEN concat(mih.order::text, '0','0')
                    ELSE concat(mip.order::text, '1',mih.order::text)
                END)::numeric as \"order\",
                mih.menu_item_id,
                mih.parent_menu_id,
                mih.icon,
                mih.route_id,
                mih.target,
                mih.link,
                mih.internal, 
                mih.params
            FROM menu_items mih
            LEFT JOIN menu_items mip ON mih.parent_menu_id = mip.menu_item_id
            WHERE mih.menu_id =:menuId and mih.active =:active
            ORDER BY 2";

        $items = Yii::$app->db->createCommand($sql, [
            ':menuId' => $this->menu_id,
            ':active' => static::STATUS_ACTIVE
        ])->queryAll();
        $resultItems = [];
        $menuOfRol = array_map(function ($data) {
            return $data->menu_item_id;
        }, static::getMenusForRol());
        foreach ($items as $item) {
            $finalParams = [];
            if ($item['params']) {
                $params =  explode(',', $item['params']);
                foreach ($params as $value) {
                    $explode = explode('=', $value);
                    $finalParams[$explode[0]] = $explode[1];
                }
            }

            if (is_null($item['parent_menu_id']) && in_array($item['menu_item_id'], $menuOfRol)) {
                $resultItems[$item['menu_item_id']] = [
                    'label' => $item['name'],
                    'url' => array_merge([$item['route_id']], $finalParams),
                    'icon' => $item['icon'],
                    'link' => $item['link'],
                    'internal' => $item['internal'],
                    'target' => $item['target']
                ];
            } elseif (in_array($item['parent_menu_id'], $menuOfRol)) {
                if (isset($resultItems[$item['parent_menu_id']]['items'])) {
                    $resultItems[$item['parent_menu_id']]['items'][] = [
                        'label' => $item['name'],
                        'url' => array_merge([$item['route_id']], $finalParams),
                    ];
                } else {
                    $resultItems[$item['parent_menu_id']]['items'] = [
                        [
                            'label' => $item['name'],
                            'url' => array_merge([$item['route_id']], $finalParams),
                        ],
                    ];
                }
                $resultItems[$item['parent_menu_id']]['url'] = 'javascript:;';
            }
        }

        return $resultItems;
    }

    /**
     * Retorna los items del menú asignados al rol
     * 
     * @return array
     */
    public static function getMenusForRol()
    {
        $result = [];
        $roles = array_map(function ($rol) {
            if (Yii::$app->user->can($rol)) {
                return $rol;
            }
        }, array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id)));

        if (!empty($roles)) {
            $result = MenuItems::findBySql("
            SELECT mei.*
            FROM menu_items mei
            INNER JOIN role_menu_items rmi ON mei.menu_item_id = rmi.menu_item_id AND rmi.role_id IN ('" . implode("','", array_values($roles)) . "') AND rmi.active = :active
            WHERE rmi.active = :active
            ", [
                ':active' => 'Y'
            ])->all();
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param [type] $index
     * @return void
     */
    public static function getTypes($index = null)
    {
        $res = [
            self::BACKEND => Yii::t('adm', 'Backend'),
            self::FRONTEND => Yii::t('adm', 'Frontend'),
            self::MOBILE => Yii::t('adm', 'Móvil')
        ];
        return $index == null ? $res : $res[$index];
    }
}

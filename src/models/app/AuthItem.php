<?php

namespace kfit\adm\models\app;

use Yii;
use yii\helpers\Inflector;

/**
 * Éste es el modelo para la tabla "auth_item".
 * 
 *
 * @package kfit\adm\models\app 
 *
 * @property string $name Define the name for auth item (roles,routes and permissions).
 * @property string $type Define item type and  take the next values:  ROL -> Roles  PER -> Permissions  ROU -> Routes
 * @property string $description Item description
 * @property string $rule_name Identifier name of auth rule
 * @property string $data Additional value that the item takes
 * @property string $active Define if it's active or not. Format : Y->Yes, N->No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH:MM:SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH:MM:SS
 * @property AuthAssignment[] $authAssignments Datos relacionados con modelo "AuthAssignment"
 * @property AuthRule $ruleName Datos relacionados con modelo "AuthRule"
 * @property AuthItemChild[] $authItemChildren Datos relacionados con modelo "AuthItemChild"
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 * @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthItem extends \kfit\adm\models\base\AuthItem
{

    /**
     * TYPE_ROL String | define el tipo de item referente al rol
     *
     * @var const
     */
    const TYPE_ROL = 'ROL';

    /**
     * TYPE_PER String | define el tipo de item referente al permiso
     *
     * @var const
     */
    const TYPE_PERMISSION = 'PER';

    /**
     * TYPE_ROUTE String | define el type de item referente a la ruta
     *
     * @var const
     */
    const TYPE_ROUTE = 'ROU';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [
            'name' => Yii::t('adm', 'Name')
        ];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return 'name';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * Obtener roles disponibles para un usuario
     *
     * @param int $userId | identificador del usuario
     * @return array[] Listado de roles disponibles
     */
    public static function getAvailableItems($userId, $type = null)
    {

        $sql = "SELECT ati.name, ati.type
                FROM auth_item ati
                WHERE ati.name not in (select ata.item_name from auth_assignment ata WHERE ata.active='Y' AND ata.user_id = :user_id) AND ati.active = 'Y'";

        $params = [':user_id' => $userId];

        if (!empty($type) && in_array($type, [
            self::TYPE_ROL,
            self::TYPE_ROUTE,
            self::TYPE_PERMISSION,
        ])) {
            $params[':type'] = $type;
            $sql .= " AND ati.type = :type ";
        }

        $sql .= " ORDER BY ati.name ASC";

        return Yii::$app->db
            ->createCommand($sql)
            ->bindValues($params)
            ->queryAll();
    }

    /**
     * Obtener roles asignados a un usuario
     *
     * @param int $userId | identificador del usuario
     * @return array[] Listado de roles asignados
     */
    public static function getAssignedItems($userId)
    {
        $sql = "SELECT ati.name, ati.type
                FROM auth_item ati
                INNER JOIN auth_assignment ata ON (ati.name = ata.item_name AND ata.active = 'Y')
                WHERE ata.user_id = :user_id AND ati.active = 'Y'
                ORDER BY ati.name ASC";
        return Yii::$app->db->createCommand($sql)
            ->bindValues([':user_id' => $userId])
            ->queryAll();
    }

    /**
     * Obtener permisos disponibles para un rol de usuario
     *
     * @param string $itemName nombre del rol al que se asignaran permisos
     * @return array[] listado de permisos disponibles para el rol actual
     */
    public static function getAvailableRoutesPermission($name = '')
    {
        $sql = "SELECT ati.name, ati.TYPE
        FROM auth_item ati
        WHERE ati.name not in (select aic.child from auth_item_child aic WHERE parent = :parent AND aic.active='Y')
        AND ati.type = 'ROU' AND ati.active = 'Y'
        ORDER BY ati.name ASC";
        return Yii::$app->db->createCommand($sql)
            ->bindValues([':parent' => $name])
            ->queryAll();
    }

    /**
     * Obtener listado de permisos asignados
     *
     * @return array[]
     */
    public static function getAssignedRoutesPermission($name = '')
    {
        $sql = "SELECT aic.child AS name FROM auth_item ait
        INNER JOIN auth_item_child aic ON (aic.parent = ait.name and aic.parent = :parent)
        WHERE aic.active = 'Y' AND ait.active = 'Y'";
        return Yii::$app->db->createCommand($sql)
            ->bindValues([':parent' => $name])
            ->queryAll();
    }

    /**
     * Obtener las rutas del sistema mediante las acciones definidas en  los controladores
     *
     * @return array []
     */
    public static function getRoutes()
    {
        $modules = [Yii::$app->id => Yii::$app];
        $modules = array_merge($modules, Yii::$app->getModules());
        $actionsRoutes = [];

        foreach ($modules as $moduleId => $moduleInstance) {
            $finalModuleId = ($moduleId == Yii::$app->id ? '' : '/' . $moduleId);
            if (!is_subclass_of($moduleInstance, \yii\base\Module::class)) {
                $moduleInstance =  Yii::$app->getModule($moduleId);
            }
            if (isset($moduleInstance->controllerNamespace) && !empty($moduleInstance->controllerNamespace)) {
                $controllersModule = \yii\helpers\FileHelper::findFiles(
                    Yii::getAlias(\str_replace('\\', '/', '@' . $moduleInstance->controllerNamespace)),
                    ['recursive' => true]
                );
                foreach ($controllersModule as $controller) {
                    $controllerId = Inflector::camel2id(substr(basename($controller), 0, -14), '-', true);
                    $controllerInstance = $moduleInstance->createControllerByID($controllerId);
                    static::processController($controllerInstance, $actionsRoutes, $finalModuleId);
                }
            }

            if (isset($moduleInstance->controllerMap)) {
                $controllersMap = $moduleInstance->controllerMap;
                foreach ($controllersMap as $controllerId => $controller) {
                    $controllerInstance = Yii::createObject($controller, [$controllerId, $moduleInstance]);
                    static::processController($controllerInstance, $actionsRoutes, $finalModuleId);
                }
            }
        }

        $result = Yii::$app->arrayHelper::merge($actionsRoutes, self::getAssignedRoutes());
        asort($result);

        return array_values($result);
    }

    /**
     * Undocumented function
     *
     * @param [type] $controllerInstance
     * @param [type] $actionsRoutes
     * @param string $moduleId
     * @return void
     */
    private static function processController($controllerInstance, &$actionsRoutes, $moduleId = '')
    {
        if ($controllerInstance) {
            $actionMap = $controllerInstance->actions();
            $actions = array_keys($actionMap);
            $methods = get_class_methods($controllerInstance);
            $methodsNew = array_map(function ($item) {
                $excludeList = ['actions'];
                if (!in_array($item, $excludeList) && preg_match('/action([A-Za-z]+)+/', $item, $actionName)) {
                    return Inflector::camel2id($actionName[1]);
                }
            }, $methods);
            $methodsNew = array_filter($methodsNew, function ($item) {
                return !is_null($item);
            });
            $methodsNew = array_values($methodsNew);
            $actions = array_merge($actions, $methodsNew);

            foreach ($actions as $actionId) {
                $route = $moduleId . '/' . $controllerInstance->id . '/' . $actionId;
                $actionsRoutes[] = ['name' => $route];
            }
        }
    }

    /**
     * Registrar las rutas seleccionadas dentro del sistema
     *
     * @return array[] $routeList | listado de rutas asignadas y disponibles
     */
    public function assignRoutes($routes)
    {
        $response = [
            'status' => true,
        ];
        if (!empty($routes)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($routes as $route) {
                    $oldModel = self::findOne(['name' => $route, 'type' => 'ROU', 'active' => 'N']);
                    if (!empty($oldModel)) {
                        $oldModel->active = 'Y';
                        $oldModel->save();
                    } else {
                        $model = new AuthItem();
                        $model->name = $route;
                        $model->type = 'ROU';
                        $model->save(false);
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'Routes' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['routes']['available'] = self::getAvailableRoutes();
                $response['routes']['assigned'] = self::getAssignedRoutes();
            }
        }
        return $response;
    }

    /**
     * Remover las rutas asignadas en la bd listado de actualizado de estas
     *
     * @param int $userId | identificador del usuario
     * @param array[] $routes | listado de roles para asignar
     * @return array[] $routesList | listado de roles asignados y disponibles
     * @return
     */
    public function revokeRoutes($routes = [])
    {
        $response = [
            'status' => true,
        ];
        if (!empty($routes)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($routes as $route) {
                    self::updateAll(
                        ['active' => Yii::$app->strings::NO],
                        [
                            'type' => 'ROU',
                            'name' => $route,
                        ]
                    );
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'routes' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['routes']['available'] = self::getAvailableRoutes();
                $response['routes']['assigned'] = self::getAssignedRoutes();
            }
        }
        return $response;
    }

    /**
     * Rutas nuevas o disponibles para  registrar
     *
     * @return array []
     */
    public static function getAvailableRoutes()
    {
        $sql = "SELECT ait.name FROM auth_item ait WHERE ait.type = 'ROU' AND ait.active = 'Y'";
        $allRoutes = Yii::$app->arrayHelper::getColumn(self::getRoutes(), 'name');
        $registeredRoutes = Yii::$app->arrayHelper::getColumn(Yii::$app->db->createCommand($sql)->queryAll(), 'name');
        $availableRoutes = [];
        foreach (array_diff($allRoutes, $registeredRoutes) as $routeAvailable) {
            $availableRoutes[] = ['name' => $routeAvailable];
        }
        return $availableRoutes;
    }

    /**
     * Retorna las rutas registradas en la bd
     *
     * @return array[]
     */
    public static function getAssignedRoutes()
    {
        $sql = "SELECT ait.name FROM auth_item ait WHERE ait.type = 'ROU' AND ait.active = 'Y'";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

    /**
     * Permite crear una nueva ruta
     *
     * @return boolean
     */
    public function createRoute($route)
    {
        $response = [];
        $model = new AuthItem();
        $model->type = 'ROU';
        $model->name = $route;
        if ($model->validate() && $model->save()) {
            $response = [
                'status' => true,
                'message' => Yii::t($this->module->id, 'It was created successfully'),
                'routes' => [
                    'available' => self::getAvailableRoutes(),
                    'assigned' => self::getAssignedRoutes(),
                ],
            ];
        } else {
            $response = [
                'status' => false,
                'message' => Yii::$app->html::errorSummary($model),
            ];
        }

        return $response;
    }

    /**
     * Permite obtener los permisos disponibles para asignar a un rol seleccionado
     *
     * @param String $role | nombre del role por el cual se realizara la búsqueda
     * @return array $itemList | lista de permisos obtenidos
     */
    public static function getAvailableRoleItems($role = '')
    {
        $sql = "SELECT ati.name, ati.type
        FROM auth_item ati
        WHERE ati.name not in
        (select aic.child from auth_item_child aic WHERE aic.active='Y' AND aic.parent = :role_name)
        AND ati.name != :role_name
        AND ati.active = :active
        ORDER BY ati.name ASC";
        return Yii::$app->db->createCommand($sql)
            ->bindValues([
                ':role_name' => $role,
                ':active' => self::STATUS_ACTIVE
            ])
            ->queryAll();
    }

    /**
     * Permite obtener los permisos asignados a un rol seleccionado
     *
     * @param String $role | nombre del rol por el cual se realizara la búsqueda
     * @return array $itemList | lista de permisos asignados
     */
    public static function getAssignedRoleItems($role = '')
    {

        $sql = "SELECT aic.child AS name, atit.type
        FROM auth_item_child aic
        INNER JOIN auth_item ati ON (ati.name = aic.parent)
        INNER JOIN auth_item atit ON (atit.name = aic.child)
        WHERE ati.name = :role_name AND aic.active = 'Y'
        ORDER BY ati.name ASC";
        return Yii::$app->db->createCommand($sql)
            ->bindValues(['role_name' => $role])
            ->queryAll();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getItemsAssignment()
    {
        $result = [
            'available' => [],
            'assigned' => []
        ];
        if ($this->type === static::TYPE_ROL) {
            $result = [
                'available' => $this->getAvailableRoleItems($this->name),
                'assigned' => $this->getAssignedRoleItems($this->name)
            ];
        } elseif ($this->type ===  static::TYPE_ROUTE) {
            $result = [
                'available' => $this->getAvailableRoutes(),
                'assigned' => $this->getAssignedRoutes()
            ];
        } elseif ($this->type ===  static::TYPE_PERMISSION) {
            $result = [
                'available' => $this->getAvailableRoutesPermission($this->name),
                'assigned' => $this->getAssignedRoutesPermission($this->name)
            ];
        }
        return  $result;
    }
}

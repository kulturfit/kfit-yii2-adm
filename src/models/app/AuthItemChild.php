<?php

namespace kfit\adm\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_item_child".
 * 
 *
 * @package kfit\adm\models\app 
 *
 * @property integer $auth_item_child_id Auth item child identifier
 * @property string $parent Item parent identifier
 * @property string $child Item child identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by  It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $child0 Datos relacionados con modelo "AuthItem"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthItemChild extends \kfit\adm\models\base\AuthItemChild
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * Metodo para asignar las rutas seleccionadas a un permiso
     *
     * @return array []
     */
    public function assignRoutesPermission($items = [], $parent)
    {
        $response = [
            'status' => true,
            'routes' => [],
        ];
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($items as $child) {
                $oldModel = AuthItemChild::findOne(
                    [
                        'parent' => $parent,
                        'child' => $child,
                    ]
                );
                if (empty($oldModel)) {
                    $modelPermission = new AuthItemChild();
                    $modelPermission->parent = $parent;
                    $modelPermission->child = $child;
                    $modelPermission->save(false);
                } else {
                    $oldModel->active = 'Y';
                    $oldModel->save(false);
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response = [
                'status' => false,
                'Routes' => [],
            ];
            throw $e;
        }
        if (isset($response['status']) && $response['status']) {
            $response['routes']['available'] = AuthItem::getAvailableRoutesPermission($parent);
            $response['routes']['assigned'] = AuthItem::getAssignedRoutesPermission($parent);
        }
        return $response;
    }

    /**
     * Remover las rutas seleccionadas de un permiso
     *
     * @param array[] $routes | listado de rutas para asignar
     */
    public function revokeRoutesPermission($routes = [], $parent)
    {
        $response = [
            'status' => true,
        ];
        if (!empty($routes)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($routes as $child) {
                    AuthItemChild::updateAll(
                        ['active' => Yii::$app->strings::NO],
                        [
                            'parent' => $parent,
                            'child' => $child,
                        ]
                    );
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'routes' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['routes']['available'] = AuthItem::getAvailableRoutesPermission($parent);
                $response['routes']['assigned'] = AuthItem::getAssignedRoutesPermission($parent);
            }
        }
        return $response;
    }

    /**
     * Metodo para asignar los items seleccionados a un rol
     *
     * @return array []
     */
    public function assignItemsToRoles($items = [], $parent)
    {
        $response = [
            'status' => true,
        ];
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($items as $child) {
                $oldModel = AuthItemChild::findOne(
                    [
                        'parent' => $parent,
                        'child' => $child,
                    ]
                );
                if (empty($oldModel)) {
                    $modelPermission = new AuthItemChild();
                    $modelPermission->parent = $parent;
                    $modelPermission->child = $child;
                    $modelPermission->save(false);
                } else {
                    $oldModel->active = 'Y';
                    $oldModel->save(false);
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response = [
                'status' => false,
                'roleItems' => [],
            ];
            throw $e;
        }
        if (isset($response['status']) && $response['status']) {
            $response['roleItems']['available'] = AuthItem::getAvailableRoleItems($parent);
            $response['roleItems']['assigned'] = AuthItem::getAssignedRoleItems($parent);
        }
        return $response;
    }

    /**
     * Remover las items(roles, permisos, rutas) seleccionados de un rol
     *
     * @param array[] $routes | listado de items asignados
     */
    public function revokeItemsToRoles($items = [], $parent)
    {
        $response = [
            'status' => true,
        ];
        if (!empty($items)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($items as $child) {
                    AuthItemChild::updateAll(
                        ['active' => Yii::$app->strings::NO],
                        [
                            'parent' => $parent,
                            'child' => $child,
                        ]
                    );
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'roleItems' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['roleItems']['available'] = AuthItem::getAvailableRoleItems($parent);
                $response['roleItems']['assigned'] = AuthItem::getAssignedRoleItems($parent);
            }
        }
        return $response;
    }
}

<?php

namespace kfit\adm\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_item".
 * 
 *
 * @package kfit\adm\models\app\Permissions 
 *
 * @property string $name Define the name for auth item (roles,routes and permissions).
 * @property string $type Define item type and  take the next values:  ROL -> Roles  PER -> Permissions  ROU -> Routes
 * @property string $description Item description
 * @property string $rule_name Identifier name of auth rule
 * @property string $data Additional value that the item takes
 * @property string $active Define if it's active or not. Format : Y->Yes, N->No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH:MM:SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH:MM:SS
 * @property AuthAssignment[] $authAssignments Datos relacionados con modelo "AuthAssignment"
 * @property AuthRule $ruleName Datos relacionados con modelo "AuthRule"
 * @property AuthItemChild[] $authItemChildren Datos relacionados con modelo "AuthItemChild"
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 * @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Permission extends \kfit\adm\models\app\AuthItem
{

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        $this->type = static::TYPE_PERMISSION;
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return 'name';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $parentGridColumns = parent::gridColumns();
        $gridColumns = [
            'name' => [
                'attribute' => 'name'
            ]
        ];
        $parentGridColumns = [];
        return Yii::$app->arrayHelper::merge($parentGridColumns, $gridColumns);
    }

    /**
     * @inheritDoc
     */
    public static function crudTitle()
    {
        return 'Permissions';
    }
}

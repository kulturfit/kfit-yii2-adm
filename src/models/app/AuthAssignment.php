<?php

namespace kfit\adm\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_assignment".
 * 
 *
 * @package kfit\adm\models\app 
 *
 * @property integer $auth_assignment_id Assignment identifier
 * @property string $item_name Role identifier
 * @property integer $user_id Identifier user
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $itemName Datos relacionados con modelo "AuthItem"
 * @property Users $user Datos relacionados con modelo "Users"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthAssignment extends \kfit\adm\models\base\AuthAssignment
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * Asignar los roles al usuario, registrarlos en la bd y retornar roles asignados y disponibles
     *
     * @param int $userId | identificador del usuario
     * @param array[] $items | listado de roles para asignar
     * @return array[] $roleList | listado de roles asignados y disponibles
     */
    public function assignItems($userId, $items = [])
    {
        $response = [
            'status' => true,
        ];
        if (!empty($items)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($items as $role) {
                    $oldModel = AuthAssignment::findOne([
                        'user_id' => $userId,
                        'item_name' => $role,
                    ]);
                    if (empty($oldModel)) {
                        $model = new AuthAssignment();
                        $model->user_id = $userId;
                        $model->item_name = $role;
                        $model->save();
                    } else {
                        $oldModel->active = 'Y';
                        $oldModel->save(true, ['active']);
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'items' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['items']['available'] = AuthItem::getAvailableItems($userId);
                $response['items']['assigned'] = AuthItem::getAssignedItems($userId);
            }
        }
        return $response;
    }

    /**
     * Remover los roles del usuario, registralo en la bd y retornar listado de roles
     * asignados y removidos
     *
     * @param int $userId | identificador del usuario
     * @param array[] $items | listado de roles para asignar
     * @return array[] $roleList | listado de roles asignados y disponibles
     * @return
     */
    public function revokeItems($userId, $items = [])
    {
        $response = [
            'status' => true,
        ];
        if (!empty($items)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($items as $role) {
                    $model = AuthAssignment::updateAll(
                        ['active' => Yii::$app->strings::NO],
                        [
                            'user_id' => $userId,
                            'item_name' => $role,
                        ]
                    );
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = [
                    'status' => false,
                    'items' => [],
                ];
                throw $e;
            }
            if (isset($response['status']) && $response['status']) {
                $response['items']['available'] = AuthItem::getAvailableItems($userId);
                $response['items']['assigned'] = AuthItem::getAssignedItems($userId);
            }
        }
        return $response;
    }
}

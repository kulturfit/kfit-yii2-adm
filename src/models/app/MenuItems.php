<?php

namespace kfit\adm\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "menu_items".
 * 
 *
 * @package kfit\adm\models\app 
 *
 * @property integer $menu_item_id Identifier menu item
 * @property integer $menu_id 
 * @property string $name Menu name
 * @property integer $parent_menu_id Parent menu identifier
 * @property string $internal Define if item is internal or external. Format Y-> Yes  N-> No
 * @property string $route_id Route identifier
 * @property string $link Link to the one that redirects the menu.
 * @property string $icon Icon associated to the menu item
 * @property integer $order Order in which the menu item is displayed
 * @property string $target Define where the content of the menu item will be displayed, format:  SELF = En la misma pestaña  BLANK = En una pestaña nueva
 * @property string $params Item parameters
 * @property string $description Description of the menu item
 * @property string $active Define if it's active or not. Format :  Y-> Yes  N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $route Datos relacionados con modelo "AuthItem"
 * @property MenuItems $parentMenu Datos relacionados con modelo "MenuItems"
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 * @property Menus $menu Datos relacionados con modelo "Menus"
 * @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class MenuItems extends \kfit\adm\models\base\MenuItems
{
    public $includeActionColumns = false;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return 'name';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $parentFormColumns = parent::formColumns();
        $parentFormColumns['description']['render'] = ['C', 'U'];
        $parentFormColumns['menu_id']['render'] = [];
        $parentFormColumns['parent_menu_id']['widget']['data'] = [];
        $parentFormColumns['link']['type'] = 'textInput';
        $formColumns = [
            'target' => [
                'widget' => [
                    'class' => \kartik\widgets\Select2::class,
                    'data' => static::getTargets(),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty(),
                    ]
                ]
            ],
            'internal' => [
                'widget' => [
                    'class' => \kartik\widgets\Select2::class,
                    'data' => Yii::$app->strings::getCondition(),
                    'options' => [
                        'placeholder' => Yii::$app->strings::getTextEmpty(),
                    ]
                ]
            ],
            'parent_menu_id' => [
                'widget' => [
                    'data' => static::getData(true, ['menu_id' => $this->menu_id])
                ]
            ]
        ];

        return Yii::$app->arrayHelper::merge($parentFormColumns, $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [
            'name' => [
                'attribute' => 'name',
                'value' => function ($model) {
                    if (empty($model->parent_menu_id)) {
                        return Yii::$app->html::tag('div', Yii::$app->html::tag('span', '', ['class' => 'fa fa-bars']) . ' ' . $model->name);
                    } else {
                        return Yii::$app->html::tag('div', Yii::$app->html::tag('span', '', ['class' => 'ml-50 fa fa-arrow-right']) . ' ' . $model->name);
                    }
                },
                'format' => 'raw',
                'visible' => true,
            ],
            'menu_id' => ['visible' => false],
            'parent_menu_id' => ['visible' => false],
            'internal' => ['visible' => false],
            'link' => ['visible' => false],
            'icon' => ['visible' => false],
            'order' => ['visible' => false],
            'route_id' => ['visible' => false],
            'target' => ['visible' => false],
            [
                'class' => 'kfit\core\base\ActionColumn',
                'template' => "{roles} {update} {delete} {restore}",
                'isModal' => true,
                'dynaModalId' => 'menu-items',
                'buttons' => [
                    'roles' => function ($url, $model, $key) {
                        return Yii::$app->html::a(Yii::$app->html::tag('span', '', ['class' => "fa fa-link"]), $url, ['onclick' => "openModalGrid(this, 'menu-items', 'update'); return false;"]);
                    },
                ],
                'controller' => '/adm/menu-items',
            ]
        ];


        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getTargets($list = true, $value = null)
    {
        $targets = [
            '_blank' => Yii::t('adm', '_blank'),
            '_self' => Yii::t('adm', '_self'),
            '_parent' => Yii::t('adm', '_parent'),
            '_top' => Yii::t('adm', '_top')
        ];

        return $list ? $targets : $targets[$value] ?? $value;
    }
}

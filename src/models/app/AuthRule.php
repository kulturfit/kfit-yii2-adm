<?php

namespace kfit\adm\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_rule".
 * 
 *
 * @package kfit\adm\models\app 
 *
 * @property string $name Rule identifier
 * @property string $data Additional value of the rule identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem[] $authItems Datos relacionados con modelo "AuthItem"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthRule extends \kfit\adm\models\base\AuthRule
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return 'name';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    public static function crudTitle()
    {
        return 'Rules';
    }
}

<?php

namespace kfit\adm\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\adm\models\app\RoleMenuItems as RoleMenuItemsModel;

/**
 * Esta clase representa las búsqueda para el modelo `kfit\adm\models\app\RoleMenuItems`.
 *
 * @package app
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S. 
 *
 */
class RoleMenuItems extends RoleMenuItemsModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['role_menu_item_id', 'menu_item_id', 'created_by', 'updated_by'], 'integer'],
            [['role_id', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'role_menu_item_id' => $this->role_menu_item_id,
            'menu_item_id' => $this->menu_item_id,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'role_id', $this->role_id])
            ->andFilterWhere(['ilike', 'active', $this->active]);

        $dataProvider->sort->defaultOrder = [static::getNameFromRelations() => SORT_ASC];

        return $dataProvider;
    }
}

<?php

namespace kfit\adm\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\adm\models\app\MenuItems as MenuItemsModel;

/**
 * Esta clase representa las búsqueda para el modelo `kfit\adm\models\app\MenuItems`.
 *
 * @package app
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S. 
 *
 */
class MenuItems extends MenuItemsModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['menu_item_id', 'menu_id', 'parent_menu_id', 'order', 'created_by', 'updated_by'], 'integer'],
            [['name', 'internal', 'route_id', 'link', 'icon', 'target', 'params', 'description', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        $query->alias('mih');
        $query->select([
            'mih.name',
            "CASE WHEN mih.parent_menu_id IS NULL THEN concat(mih.order::text, '0','0')
                ELSE concat(mip.order::text, '1',mih.order::text)
            END as \"order\"",
            'mih.parent_menu_id',
            'mih.menu_item_id',
            'mih.active'
        ]);
        $query->leftJoin('menu_items mip', 'mih.parent_menu_id = mip.menu_item_id');

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'order' => SORT_ASC,
                ],
                'attributes' => [
                    'order'
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'mih.menu_item_id' => $this->menu_item_id,
            'mih.menu_id' => $this->menu_id,
            'mih.parent_menu_id' => $this->parent_menu_id,
            'mih.order' => $this->order,
            'mih.created_by' => $this->created_by,
            'DATE(mih.created_at)' => $this->created_at,
            'mih.updated_by' => $this->updated_by,
            'DATE(mih.updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'mih.name', $this->name])
            ->andFilterWhere(['ilike', 'mih.internal', $this->internal])
            ->andFilterWhere(['ilike', 'mih.route_id', $this->route_id])
            ->andFilterWhere(['ilike', 'mih.link', $this->link])
            ->andFilterWhere(['ilike', 'mih.icon', $this->icon])
            ->andFilterWhere(['ilike', 'mih.target', $this->target])
            ->andFilterWhere(['ilike', 'mih.params', $this->params])
            ->andFilterWhere(['ilike', 'mih.description', $this->description])
            ->andFilterWhere(['ilike', 'mih.active', $this->active]);

        // $dataProvider->sort->defaultOrder = [static::getNameFromRelations() => SORT_ASC];

        return $dataProvider;
    }
}

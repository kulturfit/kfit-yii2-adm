<?php

namespace kfit\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "users".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $user_id User identifier
 * @property string $username Username with which the user logs in.
 * @property string $email User email
 * @property string $password_hash Password encrypted.
 * @property string $auth_key Random code that identifies the access key for resources by the api rest
 * @property string $password_reset_token Random code to identify when to reset the password   
 * @property string $email_key Key to validate that the email is valid
 * @property string $confirmed_email Establishes if the email is confirmed. Format  Y -> Yes  N -> No
 * @property string $blocked Establishes if the user is blocked  Y -> Yes  N -> No
 * @property string $accepted_terms It determines if the user accepts or not the terms and conditions.  Format: Y->Yes or N->No
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by  It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property Actors[] $actors Datos relacionados con modelo "Actors"
 * @property AuthAssignment[] $authAssignments Datos relacionados con modelo "AuthAssignment"
 * @property SocialNetworks[] $socialNetworks Datos relacionados con modelo "SocialNetworks"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Users extends \kfit\adm\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password_hash', 'auth_key', 'confirmed_email', 'blocked'], 'required'],
            [['password_hash', 'password_reset_token', 'email_key'], 'string'],
            [['username', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['confirmed_email', 'blocked', 'accepted_terms'], 'string', 'max' => 1],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('adm', 'Código'),
            'username' => Yii::t('adm', 'Username'),
            'email' => Yii::t('adm', 'Email'),
            'password_hash' => Yii::t('adm', 'Password hash'),
            'auth_key' => Yii::t('adm', 'Auth key'),
            'password_reset_token' => Yii::t('adm', 'Password reset token'),
            'email_key' => Yii::t('adm', 'Email key'),
            'confirmed_email' => Yii::t('adm', 'Confirmed email'),
            'blocked' => Yii::t('adm', 'Blocked'),
            'accepted_terms' => Yii::t('adm', 'Accepted terms'),
            'active' => Yii::t('adm', 'Active'),
            'created_by' => Yii::t('adm', 'Created by'),
            'created_at' => Yii::t('adm', 'Created at'),
            'updated_by' => Yii::t('adm', 'Updated by'),
            'updated_at' => Yii::t('adm', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'user_id' => Yii::t('adm', 'User identifier'),
            'username' => Yii::t('adm', 'Username with which the user logs in.'),
            'email' => Yii::t('adm', 'User email'),
            'password_hash' => Yii::t('adm', 'Password encrypted.'),
            'auth_key' => Yii::t('adm', 'Random code that identifies the access key for resources by the api rest'),
            'password_reset_token' => Yii::t('adm', 'Random code to identify when to reset the password   '),
            'email_key' => Yii::t('adm', 'Key to validate that the email is valid'),
            'confirmed_email' => Yii::t('adm', 'Establishes if the email is confirmed. format  y -> yes  n -> no'),
            'blocked' => Yii::t('adm', 'Establishes if the user is blocked  y -> yes  n -> no'),
            'accepted_terms' => Yii::t('adm', 'It determines if the user accepts or not the terms and conditions.  format: y->yes or n->no'),
            'active' => Yii::t('adm', 'Define if it\'s active or not. format : y -> yes, n -> no'),
            'created_by' => Yii::t('adm', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('adm', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('adm', ' it\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('adm', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Actors".
     *
     * @return \app\models\app\Actors
     */
    public function getActors()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\Actors::class), ['user_id' => 'user_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "AuthAssignments".
     *
     * @return \app\models\app\AuthAssignments
     */
    public function getAuthAssignments()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\AuthAssignment::class), ['user_id' => 'user_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "SocialNetworks".
     *
     * @return \app\models\app\SocialNetworks
     */
    public function getSocialNetworks()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\SocialNetworks::class), ['user_id' => 'user_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Users[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'user_id', static::getNameFromRelations());
        }
        return $query;
    }

}
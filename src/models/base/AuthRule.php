<?php

namespace kfit\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_rule".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property string $name Rule identifier
 * @property string $data Additional value of the rule identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem[] $authItems Datos relacionados con modelo "AuthItem"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthRule extends \kfit\adm\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'auth_rule';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['data'], 'string'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('adm', 'Código'),
            'data' => Yii::t('adm', 'Data'),
            'active' => Yii::t('adm', 'Active'),
            'created_by' => Yii::t('adm', 'Created by'),
            'created_at' => Yii::t('adm', 'Created at'),
            'updated_by' => Yii::t('adm', 'Updated by'),
            'updated_at' => Yii::t('adm', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'name' => Yii::t('adm', 'Rule identifier'),
            'data' => Yii::t('adm', 'Additional value of the rule identifier'),
            'active' => Yii::t('adm', 'Define if it\'s active or not. format : y -> yes, n -> no'),
            'created_by' => Yii::t('adm', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('adm', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('adm', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('adm', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "AuthItems".
     *
     * @return \app\models\app\AuthItems
     */
    public function getAuthItems()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\AuthItem::class), ['rule_name' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|AuthRule[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'name', static::getNameFromRelations());
        }
        return $query;
    }

}
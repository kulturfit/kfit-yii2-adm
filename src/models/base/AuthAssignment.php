<?php

namespace kfit\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_assignment".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $auth_assignment_id Assignment identifier
 * @property string $item_name Role identifier
 * @property integer $user_id Identifier user
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $itemName Datos relacionados con modelo "AuthItem"
 * @property Users $user Datos relacionados con modelo "Users"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthAssignment extends \kfit\adm\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['item_name'], 'string', 'max' => 64],
            [['item_name'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\AuthItem::class), 'targetAttribute' => ['item_name' => 'name'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\Users::class), 'targetAttribute' => ['user_id' => 'user_id'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'auth_assignment_id' => Yii::t('adm', 'Código'),
            'item_name' => Yii::t('adm', 'Item name'),
            'user_id' => Yii::t('adm', 'User'),
            'active' => Yii::t('adm', 'Active'),
            'created_by' => Yii::t('adm', 'Created by'),
            'created_at' => Yii::t('adm', 'Created at'),
            'updated_by' => Yii::t('adm', 'Updated by'),
            'updated_at' => Yii::t('adm', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'auth_assignment_id' => Yii::t('adm', 'Assignment identifier'),
            'item_name' => Yii::t('adm', 'Role identifier'),
            'user_id' => Yii::t('adm', 'Identifier user'),
            'active' => Yii::t('adm', 'Define if it\'s active or not. format : y -> yes, n -> no'),
            'created_by' => Yii::t('adm', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('adm', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('adm', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('adm', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "ItemName".
     *
     * @return \app\models\app\ItemName
     */
    public function getItemName()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\AuthItem::class), ['name' => 'item_name'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "User".
     *
     * @return \app\models\app\User
     */
    public function getUser()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\Users::class), ['user_id' => 'user_id'])->cache(3);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|AuthAssignment[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'auth_assignment_id', static::getNameFromRelations());
        }
        return $query;
    }

}
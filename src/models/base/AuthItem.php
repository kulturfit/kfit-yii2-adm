<?php

namespace kfit\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "auth_item".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property string $name Define the name for auth item (roles,routes and permissions).
 * @property string $type Define item type and  take the next values:  ROL -> Roles  PER -> Permissions  ROU -> Routes
 * @property string $description Item description
 * @property string $rule_name Identifier name of auth rule
 * @property string $data Additional value that the item takes
 * @property string $active Define if it's active or not. Format : Y->Yes, N->No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH:MM:SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH:MM:SS
 * @property AuthAssignment[] $authAssignments Datos relacionados con modelo "AuthAssignment"
 * @property AuthRule $ruleName Datos relacionados con modelo "AuthRule"
 * @property AuthItemChild[] $authItemChildren Datos relacionados con modelo "AuthItemChild"
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 * @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthItem extends \kfit\adm\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['type'], 'string', 'max' => 3],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\AuthRule::class), 'targetAttribute' => ['rule_name' => 'name'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('adm', 'Código'),
            'type' => Yii::t('adm', 'Type'),
            'description' => Yii::t('adm', 'Description'),
            'rule_name' => Yii::t('adm', 'Rule name'),
            'data' => Yii::t('adm', 'Data'),
            'active' => Yii::t('adm', 'Active'),
            'created_by' => Yii::t('adm', 'Created by'),
            'created_at' => Yii::t('adm', 'Created at'),
            'updated_by' => Yii::t('adm', 'Updated by'),
            'updated_at' => Yii::t('adm', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'name' => Yii::t('adm', 'Define the name for auth item (roles,routes and permissions).'),
            'type' => Yii::t('adm', 'Define item type and  take the next values:  rol -> roles  per -> permissions  rou -> routes'),
            'description' => Yii::t('adm', 'Item description'),
            'rule_name' => Yii::t('adm', 'Identifier name of auth rule'),
            'data' => Yii::t('adm', 'Additional value that the item takes'),
            'active' => Yii::t('adm', 'Define if it\'s active or not. format : y->yes, n->no'),
            'created_by' => Yii::t('adm', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('adm', 'Define the creation date and time. format: yyyy-mm-dd hh:mm:ss'),
            'updated_by' => Yii::t('adm', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('adm', 'Define the update date and time. format: yyyy-mm-dd hh:mm:ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "AuthAssignments".
     *
     * @return \app\models\app\AuthAssignments
     */
    public function getAuthAssignments()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\AuthAssignment::class), ['item_name' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "RuleName".
     *
     * @return \app\models\app\RuleName
     */
    public function getRuleName()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\AuthRule::class), ['name' => 'rule_name'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "AuthItemChildren".
     *
     * @return \app\models\app\AuthItemChildren
     */
    public function getAuthItemChildren()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\AuthItemChild::class), ['child' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "MenuItems".
     *
     * @return \app\models\app\MenuItems
     */
    public function getMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\MenuItems::class), ['route_id' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "RoleMenuItems".
     *
     * @return \app\models\app\RoleMenuItems
     */
    public function getRoleMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\RoleMenuItems::class), ['role_id' => 'name']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|AuthItem[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'name', static::getNameFromRelations());
        }
        return $query;
    }

}
<?php

namespace kfit\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "social_networks".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $social_network_id Social network identifier
 * @property integer $user_id User identifier
 * @property string $social_network_user User identifier at social network 
 * @property string $name Abbreviation of the name of the social network FAB -> Facebook; GOP -> Google Plus.
 * @property string $response Information provided by the social network
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property Users $user Datos relacionados con modelo "Users"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class SocialNetworks extends \kfit\adm\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'social_networks';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'social_network_user', 'name'], 'required'],
            [['user_id'], 'integer'],
            [['response'], 'string'],
            [['social_network_user'], 'string', 'max' => 128],
            [['name'], 'string', 'max' => 3],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\Users::class), 'targetAttribute' => ['user_id' => 'user_id'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'social_network_id' => Yii::t('adm', 'Código'),
            'user_id' => Yii::t('adm', 'User'),
            'social_network_user' => Yii::t('adm', 'Social network user'),
            'name' => Yii::t('adm', 'Name'),
            'response' => Yii::t('adm', 'Response'),
            'active' => Yii::t('adm', 'Active'),
            'created_by' => Yii::t('adm', 'Created by'),
            'created_at' => Yii::t('adm', 'Created at'),
            'updated_by' => Yii::t('adm', 'Updated by'),
            'updated_at' => Yii::t('adm', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'social_network_id' => Yii::t('adm', 'Social network identifier'),
            'user_id' => Yii::t('adm', 'User identifier'),
            'social_network_user' => Yii::t('adm', 'User identifier at social network '),
            'name' => Yii::t('adm', 'Abbreviation of the name of the social network fab -> facebook; gop -> google plus.'),
            'response' => Yii::t('adm', 'Information provided by the social network'),
            'active' => Yii::t('adm', 'Define if it\'s active or not. format : y -> yes, n -> no'),
            'created_by' => Yii::t('adm', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('adm', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('adm', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('adm', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "User".
     *
     * @return \app\models\app\User
     */
    public function getUser()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\Users::class), ['user_id' => 'user_id'])->cache(3);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|SocialNetworks[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'social_network_id', static::getNameFromRelations());
        }
        return $query;
    }

}
<?php

namespace kfit\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "menu_items".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $menu_item_id Identifier menu item
 * @property integer $menu_id 
 * @property string $name Menu name
 * @property integer $parent_menu_id Parent menu identifier
 * @property string $internal Define if item is internal or external. Format Y-> Yes  N-> No
 * @property string $route_id Route identifier
 * @property string $link Link to the one that redirects the menu.
 * @property string $icon Icon associated to the menu item
 * @property integer $order Order in which the menu item is displayed
 * @property string $target Define where the content of the menu item will be displayed, format:  SELF = En la misma pestaña  BLANK = En una pestaña nueva
 * @property string $params Item parameters
 * @property string $description Description of the menu item
 * @property string $active Define if it's active or not. Format :  Y-> Yes  N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $route Datos relacionados con modelo "AuthItem"
 * @property MenuItems $parentMenu Datos relacionados con modelo "MenuItems"
 * @property MenuItems[] $menuItems Datos relacionados con modelo "MenuItems"
 * @property Menus $menu Datos relacionados con modelo "Menus"
 * @property RoleMenuItems[] $roleMenuItems Datos relacionados con modelo "RoleMenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class MenuItems extends \kfit\adm\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'menu_items';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['menu_id', 'name', 'internal', 'order', 'target'], 'required'],
            [['menu_id', 'parent_menu_id', 'order'], 'integer'],
            [['link', 'params', 'description'], 'string'],
            [['name', 'route_id', 'icon'], 'string', 'max' => 64],
            [['internal'], 'string', 'max' => 1],
            [['target'], 'string', 'max' => 10],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\AuthItem::class), 'targetAttribute' => ['route_id' => 'name'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
            [['parent_menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\MenuItems::class), 'targetAttribute' => ['parent_menu_id' => 'menu_item_id'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\Menus::class), 'targetAttribute' => ['menu_id' => 'menu_id'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'menu_item_id' => Yii::t('adm', 'Código'),
            'menu_id' => Yii::t('adm', 'Menu'),
            'name' => Yii::t('adm', 'Name'),
            'parent_menu_id' => Yii::t('adm', 'Parent menu'),
            'internal' => Yii::t('adm', 'Internal'),
            'route_id' => Yii::t('adm', 'Route'),
            'link' => Yii::t('adm', 'Link'),
            'icon' => Yii::t('adm', 'Icon'),
            'order' => Yii::t('adm', 'Order'),
            'target' => Yii::t('adm', 'Target'),
            'params' => Yii::t('adm', 'Params'),
            'description' => Yii::t('adm', 'Description'),
            'active' => Yii::t('adm', 'Active'),
            'created_by' => Yii::t('adm', 'Created by'),
            'created_at' => Yii::t('adm', 'Created at'),
            'updated_by' => Yii::t('adm', 'Updated by'),
            'updated_at' => Yii::t('adm', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'menu_item_id' => Yii::t('adm', 'Identifier menu item'),
            'menu_id' => Yii::t('adm', ''),
            'name' => Yii::t('adm', 'Menu name'),
            'parent_menu_id' => Yii::t('adm', 'Parent menu identifier'),
            'internal' => Yii::t('adm', 'Define if item is internal or external. format y-> yes  n-> no'),
            'route_id' => Yii::t('adm', 'Route identifier'),
            'link' => Yii::t('adm', 'Link to the one that redirects the menu.'),
            'icon' => Yii::t('adm', 'Icon associated to the menu item'),
            'order' => Yii::t('adm', 'Order in which the menu item is displayed'),
            'target' => Yii::t('adm', 'Define where the content of the menu item will be displayed, format:  self = en la misma pestaña  blank = en una pestaña nueva'),
            'params' => Yii::t('adm', 'Item parameters'),
            'description' => Yii::t('adm', 'Description of the menu item'),
            'active' => Yii::t('adm', 'Define if it\'s active or not. format :  y-> yes  n -> no'),
            'created_by' => Yii::t('adm', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('adm', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('adm', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('adm', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Route".
     *
     * @return \app\models\app\Route
     */
    public function getRoute()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\AuthItem::class), ['name' => 'route_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "ParentMenu".
     *
     * @return \app\models\app\ParentMenu
     */
    public function getParentMenu()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\MenuItems::class), ['menu_item_id' => 'parent_menu_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "MenuItems".
     *
     * @return \app\models\app\MenuItems
     */
    public function getMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\MenuItems::class), ['parent_menu_id' => 'menu_item_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Definición de la relación con el modelo "Menu".
     *
     * @return \app\models\app\Menu
     */
    public function getMenu()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\Menus::class), ['menu_id' => 'menu_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "RoleMenuItems".
     *
     * @return \app\models\app\RoleMenuItems
     */
    public function getRoleMenuItems()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\RoleMenuItems::class), ['menu_item_id' => 'menu_item_id']);
		$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
		return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|MenuItems[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'menu_item_id', static::getNameFromRelations());
        }
        return $query;
    }

}
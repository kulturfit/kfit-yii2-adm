<?php

namespace kfit\adm\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "role_menu_items".
 * 
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $role_menu_item_id Identifier of a menu item associated with a role.
 * @property string $role_id Role identifier
 * @property integer $menu_item_id Menu item identifier
 * @property string $active Define if it's active or not. Format : Y -> Yes, N -> No
 * @property integer $created_by It's the identifier of the user who created the record.
 * @property string $created_at Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property integer $updated_by It's the identifier of the user who updated the record.
 * @property string $updated_at Define the update date and time. Format: YYYY-MM-DD HH: MM: SS
 * @property AuthItem $role Datos relacionados con modelo "AuthItem"
 * @property MenuItems $menuItem Datos relacionados con modelo "MenuItems"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class RoleMenuItems extends \kfit\adm\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'role_menu_items';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['role_id', 'menu_item_id'], 'required'],
            [['menu_item_id'], 'integer'],
            [['role_id'], 'string', 'max' => 64],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\AuthItem::class), 'targetAttribute' => ['role_id' => 'name'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
            [['menu_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\MenuItems::class), 'targetAttribute' => ['menu_item_id' => 'menu_item_id'], 'when' => function ($model) {
                    return !($model instanceof \kfit\core\redis\Model);
                }],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'role_menu_item_id' => Yii::t('adm', 'Código'),
            'role_id' => Yii::t('adm', 'Role'),
            'menu_item_id' => Yii::t('adm', 'Menu item'),
            'active' => Yii::t('adm', 'Active'),
            'created_by' => Yii::t('adm', 'Created by'),
            'created_at' => Yii::t('adm', 'Created at'),
            'updated_by' => Yii::t('adm', 'Updated by'),
            'updated_at' => Yii::t('adm', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'role_menu_item_id' => Yii::t('adm', 'Identifier of a menu item associated with a role.'),
            'role_id' => Yii::t('adm', 'Role identifier'),
            'menu_item_id' => Yii::t('adm', 'Menu item identifier'),
            'active' => Yii::t('adm', 'Define if it\'s active or not. format : y -> yes, n -> no'),
            'created_by' => Yii::t('adm', 'It\'s the identifier of the user who created the record.'),
            'created_at' => Yii::t('adm', 'Define the creation date and time. format: yyyy-mm-dd hh: mm: ss'),
            'updated_by' => Yii::t('adm', 'It\'s the identifier of the user who updated the record.'),
            'updated_at' => Yii::t('adm', 'Define the update date and time. format: yyyy-mm-dd hh: mm: ss'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Role".
     *
     * @return \app\models\app\Role
     */
    public function getRole()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\AuthItem::class), ['name' => 'role_id'])->cache(3);
    }

    /**
     * Definición de la relación con el modelo "MenuItem".
     *
     * @return \app\models\app\MenuItem
     */
    public function getMenuItem()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\MenuItems::class), ['menu_item_id' => 'menu_item_id'])->cache(3);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|RoleMenuItems[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if(empty($orderBy)){
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'role_menu_item_id', static::getNameFromRelations());
        }
        return $query;
    }

}
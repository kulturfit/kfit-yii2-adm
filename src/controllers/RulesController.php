<?php

namespace kfit\adm\controllers;

use Yii;
use kfit\core\base\Controller;

/**
 * Controlador RulesController implementa las acciones para el CRUD de el modelo AuthRule.
 *
 * @package kfit\adm\controllers 
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class RulesController extends Controller
{
    public $isModal = true;
    public $modelClass = \kfit\adm\models\app\AuthRule::class;
    public $searchModelClass = \kfit\adm\models\searchs\AuthRule::class;
}

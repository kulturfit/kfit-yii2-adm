<?php

namespace kfit\adm\controllers;

use Yii;
use kfit\core\base\Controller;
use kfit\sortablegridview\SortableAction;

/**
 * Controlador MenusController implementa las acciones para el CRUD de el modelo Menus.
 *
 * @package kfit\adm\controllers 
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class MenusController extends Controller
{

    /**
     * Undocumented variable
     *
     * @var array
     */
    public $actionsConfig = [
        'sortItem' => [
            'class' => SortableAction::class,
            'activeRecordClassName' => \app\models\app\MenuItems::class,
            'orderColumn' => 'order',
        ]
    ];

    public $isModal = false;
    public $formInTabs = true;
    public $tabsConfig = [
        'tab1' => [
            'label' => 'Information',
            'modelsRender' => [
                'model'
            ],
            'hasPrevious' => false,
            'hasNext' => true,
            'saveAndContinue' => true
        ],
        'tab2' => [
            'label' => 'Menu items',
            'modelKey' => 'items',
            'hasPrevious' => true,
            'hasNext' => false,
            'disabledOnCreate' => true
        ]
    ];


    // public $modelClass = \kfit\adm\models\app\Menus::class;
    public $searchModelClass = \kfit\adm\models\searchs\Menus::class;

    /**
     * @var array Undocumented variable
     */
    public $modelClass = [
        'model' => [
            'class' => \kfit\adm\models\app\Menus::class,
            'isPrimary' => true
        ],
        'items' => [
            // 'class' => \kfit\adm\modules\redis\models\app\MenuItems::class,
            // 'searchClass' => \kfit\adm\modules\redis\models\searchs\MenuItems::class,
            'redisConfig' => [
                'moduleId' => 'adm',
                'controllerId' => 'menu-items',
            ],
            'renderGrid' => true,
            'isSortableGrid' => true,
            // 'isRedis' => true,
            'class' => \kfit\adm\models\app\MenuItems::class,
            'searchClass' => \kfit\adm\models\searchs\MenuItems::class,
            'isMultiple' => true,
            'relatedModels' => [
                'model' => [
                    'isModelLoad' => true, //Solo se debe definir un modelo de carga
                    'relationName' => 'menuItems',
                    'relatedFields' => [
                        'menu_id' => 'menu_id'
                    ]
                ]
            ]
        ]
    ];

    /**
     * @inheritDoc
     */
    public function actions()
    {
        $parentActions = parent::actions();
        $parentActions['create']['routeRedirect'] = function ($model) {
            $returnUrl = 'index';

            if ($returnTab = Yii::$app->request->post('SaveAndReturnTab')) {
                if (is_array($model)) {
                    $model = $model['model'];
                }
                $returnUrl = ['/adm/menus/update', 'id' => $model->menu_id, '#' => $returnTab];
            }
            return $returnUrl;
        };
        return $parentActions;
    }
}

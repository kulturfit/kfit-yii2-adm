<?php

namespace kfit\adm\controllers;

use Yii;
use kfit\core\base\Controller;

/**
* Controlador AuthItemController implementa las acciones para el CRUD de el modelo AuthItem.
*
* @package kfit\adm\controllers 
*
* @property string $model Ruta del modelo principal.
* @property string $searchModel Ruta del modelo para la búsqueda.
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright Copyright (c) 2020 KulturFit S.A.S.
* @version 0.0.1
* @since 1.0.0
*/
class AuthItemController extends Controller
{
    public $isModal = true;
    public $modelClass = \kfit\adm\models\app\AuthItem::class;
    public $searchModelClass = \kfit\adm\models\searchs\AuthItem::class;
}
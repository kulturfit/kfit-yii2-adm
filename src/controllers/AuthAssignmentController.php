<?php

namespace kfit\adm\controllers;

use Yii;
use kfit\core\base\Controller;
use yii\web\Response;

/**
 * Controlador AuthAssignmentController implementa las acciones para el CRUD de el modelo AuthAssignment.
 *
 * @package kfit\adm\controllers 
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class AuthAssignmentController extends Controller
{
    public $isModal = true;
    public $modelClass = \kfit\adm\models\app\AuthAssignment::class;
    public $searchModelClass = \kfit\adm\models\searchs\AuthAssignment::class;

    /**
     * Asignar rol(es) a un usuario y registrarlo
     * 
     * @param int $id | identificador del usuario  
     * @return array[] $roleList | listado de roles actualizado 
     */
    public function actionAssign($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $model = Yii::createObject($this->modelClass);
        return $model->assignItems($id, $items);
    }

    /**
     * Remover rol(es) a un usuario y registrarlo
     * 
     * @param int $id | identificador del usuario  
     * @return array[] $roleList | listado de roles actualizado 
     */
    public function actionRevoke($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $model = Yii::createObject($this->modelClass);
        return $model->revokeItems($id, $items);
    }
}

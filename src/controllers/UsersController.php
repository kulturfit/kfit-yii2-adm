<?php

namespace kfit\adm\controllers;

use Yii;
use kfit\core\base\Controller;

/**
 * Controlador UsersController implementa las acciones para el CRUD de el modelo Users.
 *
 * @package kfit\adm\controllers 
 *
 * @property string $modelClass Ruta del modelo principal.
 * @property string $searchModelClass Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S. 
 *
 */
class UsersController extends Controller
{
    public $isModal = false;
    public $formInTabs = true;
    public $tabsConfig = [
        'tab1' => [
            'label' => 'Information',
            'renderModels' => ['model'],
            'hasPrevious' => false,
            'hasNext' => true,
            'saveAndContinue' => true
        ],
        'tab2' => [
            'label' => 'Assignments',
            'renderModels' => ['model'],
            'viewName' => '_assignments',
            'hasPrevious' => true,
            'disabledOnCreate' => true
        ]
    ];
    public $searchModelClass = \kfit\adm\models\searchs\Users::class;

    public $modelClass = [
        'model' => [
            'class' => \kfit\adm\models\app\Users::class,
            'isPrimary' => true,
            'configNewInstance' => [
                'scenario' => \kfit\adm\models\app\Users::SCENARIO_CREATE
            ],
            'configUpdateInstance' => [
                'scenario' => \kfit\adm\models\app\Users::SCENARIO_UPDATE,
            ]
        ]
    ];
}

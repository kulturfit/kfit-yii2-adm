<?php

namespace kfit\adm\controllers;

use kfit\adm\models\app\AuthItemChild;
use Yii;
use kfit\core\base\Controller;
use yii\web\Response;

/**
 * Controlador RoutesController implementa las acciones para el CRUD de el modelo Routes.
 *
 * @package kfit\adm\controllers 
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class RoutesController extends Controller
{

    public $modelClass = \kfit\adm\models\app\Route::class;
    public $searchModelClass = \kfit\adm\models\searchs\Route::class;

    /**
     * Asignar las rutas del sistema par almacenarlas en la bd
     * 
     * @return array []
     * 
     */
    public function actionAssign()
    {
        $modelAuth = Yii::createObject($this->modelClass);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $routes = Yii::$app->getRequest()->post('routes', []);

        return $modelAuth->assignRoutes($routes);
    }

    /**
     * Remover las rutas asignadas y almacenadas en la bd
     * 
     * @return array []
     */
    public function actionRevoke()
    {
        $modelAuth = Yii::createObject($this->modelClass);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $routes = Yii::$app->getRequest()->post('routes', []);
        return $modelAuth->revokeRoutes($routes);
    }

    /**
     * Permite obtener el listado actualizado de rutas asignads y disponibles
     * 
     * 
     */
    public function actionRefresh()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Yii::createObject($this->modelClass);
        $response = [
            'routes' => [
                'available' => $model->getAvailableRoutes(),
                'assigned' => $model->getAssignedRoutes()
            ]
        ];
        return $response;
    }

    /**
     * Permite crear una nueva ruta
     * 
     * @return boolean
     */
    public function actionCreateRoute()
    {
        $route = Yii::$app->request->post('route');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $model = Yii::createObject($this->modelClass);

        $result = $model->createRoute($route);
        if ($result['status']) {
            $response = [
                'status' => true,
                'routes' => $result['routes'],
                'message' => Yii::t('adm', $result['message'])
            ];
        } else {
            $response = [
                'status' => false,
                'message' => Yii::t('adm', $result['message'])
            ];
        }
        return $response;
    }
}

<?php

namespace kfit\adm\controllers;

use kfit\adm\models\app\AuthItemChild;
use Yii;
use kfit\core\base\Controller;
use yii\web\Response;

/**
 * Controlador RolesController implementa las acciones para el CRUD de el modelo Role.
 *
 * @package kfit\adm\controllers 
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class RolesController extends Controller
{
    public $isModal = false;
    public $formInTabs = true;
    public $tabsConfig = [
        'tab1' => [
            'label' => 'Information',
            'renderModels' => ['model'],
            'hasPrevious' => false,
            'hasNext' => true,
        ],
        'tab2' => [
            'label' => 'Assignments',
            'renderModels' => ['model'],
            'viewName' => '_assignments',
            'hasPrevious' => true,
            'disabledOnCreate' => true
        ]
    ];
    public $modelClass = \kfit\adm\models\app\Role::class;
    public $searchModelClass = \kfit\adm\models\searchs\Role::class;

    /**
     * Asigna los permisos o items seleccionados a determinado rol
     * 
     * @return array[]
     */
    public function actionAssign()
    {
        $modelAssign = Yii::createObject(AuthItemChild::class);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $parent = Yii::$app->request->post('parent');
        return $modelAssign->assignItemsToRoles($items, $parent);
    }
    /**
     * Remover los permisos o items de un rol determinado
     *   
     * @return array[] $response | listado de rutas actualizado 
     */
    public function actionRevoke()
    {
        $modelAssign = Yii::createObject(AuthItemChild::class);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $items = Yii::$app->getRequest()->post('items', []);
        $parent = Yii::$app->request->post('parent');
        return $modelAssign->revokeItemsToRoles($items, $parent);
    }
}

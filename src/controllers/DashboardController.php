<?php

namespace kfit\adm\controllers;

use yii\web\Controller;

/**
 * Controlador DashboardController permite visualizar la opciones disponibles en el módulo.
 *
 * @package kfit\adm\controllers\DashboardController
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class DashboardController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

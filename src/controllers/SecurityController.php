<?php

namespace kfit\adm\controllers;

use Yii;
use kfit\adm\components\actions\LoginAction;
use kfit\adm\components\actions\ChangePasswordAction;
use kfit\adm\components\actions\RegisterAction;
use kfit\adm\components\actions\RestorePasswordAction;
use kfit\adm\components\actions\SessionExpiredAction;
use kfit\adm\components\actions\ActiveAccountAction;
use kfit\adm\components\DynamicModel;
use kfit\core\base\Controller;

/**
 * SecurityController implementa las acciones para manejo de la sesión y control de acceso al sistema.
 *
 * @package kfit\adm
 * @subpackage controllers
 * @category Controllers
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class SecurityController extends Controller
{

    public $isModal = false;
    public $defaultAction = 'login';

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'login' => [
                'class' => LoginAction::class,
                'isModal' => $this->isModal
            ],
            'change-password' => [
                'class' => ChangePasswordAction::class,
                'isModal' => $this->isModal,
            ],
            'register' => [
                'class' => RegisterAction::class,
                'isModal' => $this->isModal,
            ],
            'restore-password' => [
                'class' => RestorePasswordAction::class,
                'isModal' => $this->isModal,
            ],
            'session-expired' => [
                'class' => SessionExpiredAction::class,
                'isModal' => $this->isModal,
            ],
            'active-account' => [
                'class' => ActiveAccountAction::class,
                'isModal' => $this->isModal,
            ],
            'change-password' => [
                'class' => ChangePasswordAction::class,
                'isModal' => $this->isModal,
            ]
        ];
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

<?php

namespace kfit\adm\controllers;

use kfit\adm\models\app\RoleMenuItems;
use Yii;
use kfit\core\base\Controller;
use kfit\core\components\DynamicModel;
use yii\web\Response;

/**
 * Controlador MenuItemsController implementa las acciones para el CRUD de el modelo MenuItems.
 *
 * @package kfit\adm\controllers 
 *
 * @property string $modelClass Ruta del modelo principal.
 * @property string $searchModelClass Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S. 
 *
 */
class MenuItemsController extends Controller
{
    public $isModal = true;
    public $modelClass = \kfit\adm\models\app\MenuItems::class;
    public $searchModelClass = \kfit\adm\models\searchs\MenuItems::class;

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function actionRoles($id)
    {
        $roles = Yii::$app->arrayHelper::getColumn(RoleMenuItems::find()
            ->select('role_id')
            ->where([
                'menu_item_id' => $id,
                RoleMenuItems::STATUS_COLUMN => RoleMenuItems::STATUS_ACTIVE,
            ])
            ->asArray()
            ->all(), 'role_id');

        $model = new DynamicModel([
            'menu_item_id' => $id,
            'roles' => $roles
        ]);

        $model->setAttributeLabels([
            'roles' => Yii::t('adm', 'Roles')
        ]);
        $model->addRule(['menu_item_id', 'roles'], 'required');

        unset($roles);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            RoleMenuItems::updateAll([
                RoleMenuItems::STATUS_COLUMN => RoleMenuItems::STATUS_INACTIVE,
            ], ['menu_item_id' => $model->menu_item_id]);

            foreach ($model->roles as $role) {
                $modelNew = RoleMenuItems::find()
                    ->where([
                        'role_id' => $role,
                        'menu_item_id' => $model->menu_item_id,
                    ])
                    ->one();

                if (!$modelNew) {
                    $modelNew = Yii::createObject([
                        'class' => RoleMenuItems::class,
                        'role_id' => $role,
                        'menu_item_id' => $model->menu_item_id
                    ]);
                }

                $modelNew->active = RoleMenuItems::STATUS_ACTIVE;
                $modelNew->save();
            }

            $res['state'] = 'success';
            $res['message'] = Yii::t('adm', 'It was created successfully');

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax(
                '_roles',
                [
                    'model' => $model,
                ]
            );
        }
    }
}

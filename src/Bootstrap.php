<?php

namespace kfit\adm;

use Yii;

use yii\base\Application;
use yii\base\BootstrapInterface;
use kfit\adm\models\base\AuthAssignment;
use kfit\adm\models\base\AuthItem;
use kfit\adm\models\base\AuthItemChild;
use kfit\adm\models\base\AuthRule;
use kfit\adm\models\base\MenuItems;
use kfit\adm\models\base\Menus;
use kfit\adm\models\base\RoleMenuItems;
use kfit\adm\models\base\SocialNetworks;
use kfit\adm\models\base\Users;

use kfit\adm\models\searchs\AuthAssignment as AuthAssignmentSearch;
use kfit\adm\models\searchs\AuthItem as AuthItemSearch;
use kfit\adm\models\searchs\AuthItemChild as AuthItemChildSearch;
use kfit\adm\models\searchs\MenuItems as MenuItemsSearch;
use kfit\adm\models\searchs\Menus as MenusSearch;
use kfit\adm\models\searchs\Users as UsersSearch;

use kfit\adm\models\forms\Login;

/**
 * Class Bootstrap
 * @package kfit\yii2-adm
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @var array
     */
    private $_modelMap = [
        'base' => [
            'AuthAssignment' => AuthAssignment::class,
            'AuthItem' => AuthItem::class,
            'AuthItemChild' => AuthItemChild::class,
            'AuthRule' => AuthRule::class,
            'MenuItems' => MenuItems::class,
            'Menus' => Menus::class,
            'RoleMenuItems' => RoleMenuItems::class,
            'SocialNetworks' => SocialNetworks::class,
            'Users' => Users::class,
        ],
        'searchs' => [
            'AuthAssignment' => AuthAssignmentSearch::class,
            'AuthItem' => AuthItemSearch::class,
            'AuthItemChild' => AuthItemChildSearch::class,
            'MenuItems' => MenuItemsSearch::class,
            'Menus' => MenusSearch::class,
            'Users' => UsersSearch::class,
        ],
        'task' => [],
        'forms' => [
            'Login' => Login::class
        ],
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('adm')) {
            $app->setModule('adm', 'kfit\adm\Module');
        }

        $this->overrideViews($app, 'adm');
        $this->overrideModels($app, 'adm');

    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app, $moduleId)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getView()->theme->pathMap["@kfit/{$moduleId}/views"] = "@app/views/{$moduleId}";
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideModels($app, $moduleId)
    {
        /**
         * @var Module $module
         * @var ActiveRecord $modelName
         */
        if ($app->hasModule($moduleId) && ($module = $app->getModule($moduleId)) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $pathModel => $modelMap) {
                foreach ($modelMap as $name => $definition) {
                    $class = "kfit\\{$moduleId}\\models\\{$pathModel}\\" . $name;
                    Yii::$container->set($class, $definition);
                    $modelName = is_array($definition) ? $definition['class'] : $definition;
                    $module->modelMap[$name] = $modelName;
                }
            }
        }
    }
}

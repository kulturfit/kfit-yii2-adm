<?php
namespace kfit\adm\migrations;
 /**
 * Migración m190225_230103_Menus implementa las acciones para la creación de la tabla menus.
 *
 * @package kfit\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */

class m190225_230103_Menus extends \yii\db\Migration
{
    public $tableName = 'menus';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'menu_id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'type' => $this->char(1),
            'position' => $this->string(64)->notNull(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'menu_id', "Menu identifier");

        $this->addCommentOnColumn($this->tableName, 'name', "Menu name");

        $this->addCommentOnColumn($this->tableName, 'description', "Menu's description");

        $this->addCommentOnColumn($this->tableName, 'type', "Define the menu type:

B-> Backend
F-> Frontend
M -> Movil");

        $this->addCommentOnColumn($this->tableName, 'position', "Define the location of the menu");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format : Y -> Yes, N -> No");

        $this->addCommentOnColumn($this->tableName, 'created_by', "It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', "It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH: MM: SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

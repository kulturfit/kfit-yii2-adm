<?php
namespace kfit\adm\migrations;
 /**
 * Migración m190225_230104_MenuItems implementa las acciones para la creación de la tabla menu_items.
 *
 * @package kfit\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */

class m190225_230104_MenuItems extends \yii\db\Migration
{
    public $tableName = 'menu_items';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'menu_item_id' => $this->primaryKey(),
            'menu_id' => $this->integer()->notNull(),
            'name' => $this->string(64)->notNull(),
            'parent_menu_id' => $this->integer(),
            'internal' => $this->char(1)->notNull(),
            'route_id' => $this->string(64),
            'link' => $this->text(),
            'icon' => $this->string(64),
            'order' => $this->smallInteger()->notNull(),
            'target' => $this->string(10)->notNull(),
            'params' => $this->text(),
            'description' => $this->text(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[route_id]]) REFERENCES auth_item ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[parent_menu_id]]) REFERENCES menu_items ([[menu_item_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[menu_id]]) REFERENCES menus ([[menu_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'menu_item_id', "Identifier menu item");

        $this->addCommentOnColumn($this->tableName, 'menu_id', "");

        $this->addCommentOnColumn($this->tableName, 'name', "Menu name");

        $this->addCommentOnColumn($this->tableName, 'parent_menu_id', "Parent menu identifier");

        $this->addCommentOnColumn($this->tableName, 'internal', "Define if item is internal or external. Format Y-> Yes  N-> No");

        $this->addCommentOnColumn($this->tableName, 'route_id', "Route identifier");

        $this->addCommentOnColumn($this->tableName, 'link', "Link to the one that redirects the menu.");

        $this->addCommentOnColumn($this->tableName, 'icon', "Icon associated to the menu item");

        $this->addCommentOnColumn($this->tableName, 'order', "Order in which the menu item is displayed");

        $this->addCommentOnColumn($this->tableName, 'target', "Define where the content of the menu item will be displayed, format:
SELF = En la misma pestaña
BLANK = En una pestaña nueva");

        $this->addCommentOnColumn($this->tableName, 'params', "Item parameters");

        $this->addCommentOnColumn($this->tableName, 'description', "Description of the menu item");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format :
Y-> Yes
N -> No");

        $this->addCommentOnColumn($this->tableName, 'created_by', "It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', "It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH: MM: SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

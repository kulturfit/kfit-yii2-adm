<?php
namespace kfit\adm\migrations;
 /**
 * Migración m190225_230104_RoleMenuItems implementa las acciones para la creación de la tabla role_menu_items.
 *
 * @package kfit\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */

class m190225_230104_RoleMenuItems extends \yii\db\Migration
{
    public $tableName = 'role_menu_items';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'role_menu_item_id' => $this->primaryKey(),
            'role_id' => $this->string(64)->notNull(),
            'menu_item_id' => $this->integer()->notNull(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[role_id]]) REFERENCES auth_item ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[menu_item_id]]) REFERENCES menu_items ([[menu_item_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'role_menu_item_id', "Identifier of a menu item associated with a role.");

        $this->addCommentOnColumn($this->tableName, 'role_id', "Role identifier");

        $this->addCommentOnColumn($this->tableName, 'menu_item_id', "Menu item identifier");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format : Y -> Yes, N -> No");

        $this->addCommentOnColumn($this->tableName, 'created_by', "It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', "It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH: MM: SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

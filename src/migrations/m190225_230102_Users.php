<?php
namespace kfit\adm\migrations;
 /**
 * Migración m190225_230102_Users implementa las acciones para la creación de la tabla users.
 *
 * @package kfit\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class m190225_230102_Users extends \yii\db\Migration
{
    public $tableName = 'users';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'user_id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'password_hash' => $this->text()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_reset_token' => $this->text(),
            'email_key' => $this->text(),
            'confirmed_email' => $this->char(1)->notNull(),
            'blocked' => $this->char(1)->notNull(),
            'accepted_terms' => $this->char(1)->notNull()->defaultValue('N'),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'user_id', "User identifier");

        $this->addCommentOnColumn($this->tableName, 'username', "Username with which the user logs in.");

        $this->addCommentOnColumn($this->tableName, 'email', "User email");

        $this->addCommentOnColumn($this->tableName, 'password_hash', "Password encrypted.");

        $this->addCommentOnColumn($this->tableName, 'auth_key', "Random code that identifies the access key for resources by the api rest");

        $this->addCommentOnColumn($this->tableName, 'password_reset_token', "Random code to identify when to reset the password
 ");

        $this->addCommentOnColumn($this->tableName, 'email_key', "Key to validate that the email is valid");

        $this->addCommentOnColumn($this->tableName, 'confirmed_email', "Establishes if the email is confirmed. Format
Y -> Yes
N -> No");

        $this->addCommentOnColumn($this->tableName, 'blocked', "Establishes if the user is blocked
Y -> Yes
N -> No");

        $this->addCommentOnColumn($this->tableName, 'accepted_terms', "It determines if the user accepts or not the terms and conditions.
Format: Y->Yes or N->No");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format : Y -> Yes, N -> No");

        $this->addCommentOnColumn($this->tableName, 'created_by', "It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', " It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH: MM: SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

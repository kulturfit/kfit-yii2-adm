<?php
namespace kfit\adm\migrations;
/**
 * Migración m190225_230101_AuthItem implementa las acciones para la creación de la tabla auth_item.
 *
 * @package kfit\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class m190225_230101_AuthItem extends \yii\db\Migration
{
    public $tableName = 'auth_item';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'name' => $this->string(64)->notNull(),
            'type' => $this->char(3)->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'PRIMARY KEY ([[name]])',
            'FOREIGN KEY ([[rule_name]]) REFERENCES auth_rule ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'name', "Define the name for auth item (roles,routes and permissions).");

        $this->addCommentOnColumn($this->tableName, 'type', "Define item type and  take the next values:
ROL -> Roles
PER -> Permissions
ROU -> Routes");

        $this->addCommentOnColumn($this->tableName, 'description', "Item description");

        $this->addCommentOnColumn($this->tableName, 'rule_name', "Identifier name of auth rule");

        $this->addCommentOnColumn($this->tableName, 'data', "Additional value that the item takes");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format : Y->Yes, N->No");

        $this->addCommentOnColumn($this->tableName, 'created_by', "It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH:MM:SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', "It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH:MM:SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}

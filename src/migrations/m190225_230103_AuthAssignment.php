<?php
namespace kfit\adm\migrations;
 /**
 * Migración m190225_230103_AuthAssignment implementa las acciones para la creación de la tabla auth_assignment.
 *
 * @package kfit\adm
 * @subpackage migrations
 * @category Migrations
 *
 * @property string $tableName Nombre de la tabla a generar.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class m190225_230103_AuthAssignment extends \yii\db\Migration
{
    public $tableName = 'auth_assignment';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'auth_assignment_id' => $this->primaryKey(),
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[item_name]]) REFERENCES auth_item ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[user_id]]) REFERENCES users ([[user_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'auth_assignment_id', "Assignment identifier");

        $this->addCommentOnColumn($this->tableName, 'item_name', "Role identifier");

        $this->addCommentOnColumn($this->tableName, 'user_id', "Identifier user");

        $this->addCommentOnColumn($this->tableName, 'active', "Define if it's active or not. Format : Y -> Yes, N -> No");

        $this->addCommentOnColumn($this->tableName, 'created_by', "It's the identifier of the user who created the record.");

        $this->addCommentOnColumn($this->tableName, 'created_at', "Define the creation date and time. Format: YYYY-MM-DD HH: MM: SS");

        $this->addCommentOnColumn($this->tableName, 'updated_by', "It's the identifier of the user who updated the record.");

        $this->addCommentOnColumn($this->tableName, 'updated_at', "Define the update date and time. Format: YYYY-MM-DD HH: MM: SS");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
